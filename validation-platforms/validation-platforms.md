# Validation Platforms

| <div align="center">Platform ID</div> | <div align="center">Operator</div>            | <div align="center">Status</div>         | <div align="center">Current Sylva Flavor</div>   | <div align="center">Hardware provider</div> | <div align="center">Hardware equipment</div>                                                                                                                                                                                                                 |
|:-------------:|:----------------------:|:------------------:|:------------------------:|:-------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| TEF-1       | Telefónica           | READY            | OpenStack              | HPE + Intel       | 4 x HPE ProLiant DL380 Gen10 (72 vCPUs, 384GB RAM, 878GB Local Storage) <br><br> 2 x HPE ProLiant DL380 Gen10 (72 vCPUs, 576GB RAM, 731GB Local Storage) <br><br> 2 x Intel R2208WFTZSR (96 vCPUs, 192GB RAM, 438GB Local Storage) |
| TEF-2       | Telefónica           | UNDER DEPLOYMENT | OpenStack              | Dell Inc.         | 2 x Dell PowerEdge R350 (8 vCPUs, 32GB RAM, 480GB Local Storage) <br><br> 9 x Dell PowerEdge R650xs (112 vCPUs, 256GB RAM, 1920GB Local Storage)                                                                                   |
| ORA-1       | Orange               | READY            | Bare Metal             | Dell Inc.         | 5 x Dell PowerEdge R640 (112 vCPUs, 384GB RAM, 0GB Local Storage)                                                                                                                                                                  |
| ORA-2       | Orange               | READY            | OpenStack + Bare Metal | Dell Inc. + HPE   | Gathering information                                                                                                                                                                                                              |
| ORA-3       | Orange               | READY            | Bare Metal             | Lenovo            | 6 x Lenovo Server SR650 V1 (64 vCPUs, 384GB RAM, 400GB Local Storage) <br><br> 7 x Lenovo Server SR630 V1 (64 vCPUs, 384GB RAM, 400GB Local Storage)                                                                              |
| TIM-1       | TIM (Telecom Italia) | UNDER DEPLOYMENT | Bare Metal             | Dell Inc.         | TBD                                                                                                                                                                                                                                |

# Main Hardware Providers

|  ![](assets/dell-logo.png)  |  ![](assets/hpe-logo.png)  |
|:---------------------------:|:--------------------------:|
| ![](assets/lenovo-logo.png) | ![](assets/intel-logo.png) |
