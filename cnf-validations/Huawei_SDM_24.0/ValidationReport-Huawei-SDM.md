# Validation Report for Huawei Subscriber Data Management (SDM) UDM 24.0

- Date: **28 - March - 2024**

- Validation Responsible:  **Vincent Deenen (Orange)**

- Validation Team:
  * **Vincent Deenen (Orange)**
  * **Guy Laverdure (Orange)**
  * **Fabrice Permangant (Orange)**
  * **Fran�ois Th�venot (Orange)**

- Result: PASSED

- Comments: **Report of successful onboarding and testing of Huawei SDM on Orange Telco Cloud (OTC) CaaS MVP2.0.1 based on Sylva 0.2**

## Validated Workload

- Provider of CNF:  **Huawei**

- Name of CNF: **Subscriber Data Management (SDM)**

- Version of CNF: **UDM 24.0**

- Platform capabilities required:
  
  * [ ] CPU pinning
  * [x] HugePages
  * [x] Multus CNI
  * [x] SR-IOV
  * [ ] PTP
  * [ ] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI

## Validation Platform

- Sylva version: **MVP2.0.1**

- Validation environment code: **Name to be defined**

- Flavour used for validation:
  
  * [ ] BareMetal + Vanilla K8s
  * [x] BareMetal + RKE2
  * [ ] OpenStack + Vanilla K8s
  * [ ] OpenStack + RKE2
  * [ ] VMware stack + Vanilla K8s
  * [ ] VMware stack + RKE2

- Hardware used for the platform (servers):

  - SERVER 1: ProLiant DL360 Gen10, Intel(R) Xeon(R) Gold 6240R CPU @ 2.40GHz, 24/24 cores; 48 threads, 768 GB  RAM  2 x 480 GB SSD, 2 x 4,1 TB NVME, Host Operating System: Ubuntu 22.04
  - SERVER 2: ProLiant DL360 Gen10, Intel(R) Xeon(R) Gold 6240R CPU @ 2.40GHz, 24/24 cores; 48 threads, 768 GB  RAM  2 x 480 GB SSD, 2 x 4,1 TB NVME, Host Operating System: Ubuntu 22.04
  - SERVER 3: ProLiant DL360 Gen10, Intel(R) Xeon(R) Gold 6240R CPU @ 2.40GHz, 24/24 cores; 48 threads, 768 GB  RAM  2 x 480 GB SSD, 2 x 4,1 TB NVME, Host Operating System: Ubuntu 22.04
  - SERVER 4: ProLiant DL360 Gen10, Intel(R) Xeon(R) Gold 6240R CPU @ 2.40GHz, 24/24 cores; 48 threads, 768 GB  RAM  2 x 480 GB SSD, 2 x 4,1 TB NVME, Host Operating System: Ubuntu 22.04
  - SERVER 5: ProLiant DL360 Gen10, Intel(R) Xeon(R) Gold 6240R CPU @ 2.40GHz, 24/24 cores; 48 threads, 768 GB  RAM  2 x 480 GB SSD, 2 x 4,1 TB NVME, Host Operating System: Ubuntu 22.04
  - SERVER 6: ProLiant DL360 Gen10, Intel(R) Xeon(R) Gold 6240R CPU @ 2.40GHz, 24/24 cores; 48 threads, 768 GB  RAM  2 x 480 GB SSD, 2 x 4,1 TB NVME, Host Operating System: Ubuntu 22.04
  
## Validation Evidences

### Evidence of Workload Deployment
```
root@first-workload-cluster-control-plane-c26hf:~# kubectl get all -A
NAMESPACE                  NAME                                                                         READY   STATUS             RESTARTS            AGE
calico-system              pod/calico-kube-controllers-7498db8d8b-wjld9                                 1/1     Running            0                   64d
calico-system              pod/calico-node-2z2lr                                                        1/1     Running            451 (38d ago)       93d
calico-system              pod/calico-node-7bnk8                                                        1/1     Running            5 (63d ago)         93d
calico-system              pod/calico-node-bbfm4                                                        1/1     Running            8 (58d ago)         93d
calico-system              pod/calico-node-dlx7v                                                        1/1     Running            14 (58d ago)        93d
calico-system              pod/calico-node-g8lq9                                                        1/1     Running            7 (58d ago)         93d
calico-system              pod/calico-node-r5jpv                                                        1/1     Running            10 (63d ago)        93d
calico-system              pod/calico-typha-8564dc77bd-fkpkl                                            1/1     Running            0                   58d
calico-system              pod/calico-typha-8564dc77bd-rcqsw                                            1/1     Running            0                   8d
cattle-fleet-system        pod/fleet-agent-847ffcbbb-c87m4                                              1/1     Running            0                   16d
cattle-monitoring-system   pod/alertmanager-rancher-monitoring-alertmanager-0                           2/2     Running            0                   64d
cattle-monitoring-system   pod/prometheus-rancher-monitoring-prometheus-0                               3/3     Running            0                   64d
cattle-monitoring-system   pod/rancher-monitoring-grafana-586df56bff-dmtq8                              3/3     Running            0                   64d
cattle-monitoring-system   pod/rancher-monitoring-kube-state-metrics-77ddfd789b-zh4g8                   1/1     Running            0                   64d
cattle-monitoring-system   pod/rancher-monitoring-operator-ff6cfcc76-2bsm5                              1/1     Running            0                   64d
cattle-monitoring-system   pod/rancher-monitoring-prometheus-adapter-79d8db9697-6cxb9                   1/1     Running            0                   64d
cattle-monitoring-system   pod/rancher-monitoring-prometheus-node-exporter-2hxl5                        1/1     Running            6 (63d ago)         93d
cattle-monitoring-system   pod/rancher-monitoring-prometheus-node-exporter-9hk2g                        1/1     Running            7 (5d1h ago)        93d
cattle-monitoring-system   pod/rancher-monitoring-prometheus-node-exporter-hfkfn                        1/1     Running            6 (41d ago)         93d
cattle-monitoring-system   pod/rancher-monitoring-prometheus-node-exporter-kd6jh                        1/1     Running            9 (10d ago)         93d
cattle-monitoring-system   pod/rancher-monitoring-prometheus-node-exporter-pzp2m                        1/1     Running            10 (38d ago)        93d
cattle-monitoring-system   pod/rancher-monitoring-prometheus-node-exporter-w5tpr                        1/1     Running            2030 (38d ago)      93d
cattle-sriov-system        pod/sriov-574d587f59-9vtq4                                                   1/1     Running            0                   64d
cattle-sriov-system        pod/sriov-device-plugin-7gr4w                                                1/1     Running            0                   58d
cattle-sriov-system        pod/sriov-device-plugin-85n48                                                1/1     Running            0                   58d
cattle-sriov-system        pod/sriov-device-plugin-hxpvq                                                1/1     Running            0                   58d
cattle-sriov-system        pod/sriov-device-plugin-r7cq9                                                1/1     Running            0                   58d
cattle-sriov-system        pod/sriov-device-plugin-smtv8                                                1/1     Running            0                   58d
cattle-sriov-system        pod/sriov-device-plugin-tshxq                                                1/1     Running            0                   58d
cattle-sriov-system        pod/sriov-network-config-daemon-5cvf5                                        3/3     Running            9 (63d ago)         84d
cattle-sriov-system        pod/sriov-network-config-daemon-dl68h                                        3/3     Running            6 (63d ago)         84d
cattle-sriov-system        pod/sriov-network-config-daemon-l59xj                                        3/3     Running            6 (58d ago)         84d
cattle-sriov-system        pod/sriov-network-config-daemon-ms4zf                                        3/3     Running            6 (58d ago)         84d
cattle-sriov-system        pod/sriov-network-config-daemon-np84t                                        3/3     Running            6 (63d ago)         84d
cattle-sriov-system        pod/sriov-network-config-daemon-s6l96                                        3/3     Running            9 (58d ago)         84d
cattle-sriov-system        pod/sriov-rancher-nfd-master-7468d5b9c8-8slch                                1/1     Running            0                   64d
cattle-sriov-system        pod/sriov-rancher-nfd-worker-4pxmt                                           1/1     Running            63 (58d ago)        93d
cattle-sriov-system        pod/sriov-rancher-nfd-worker-gtn94                                           1/1     Running            102 (58d ago)       93d
cattle-sriov-system        pod/sriov-rancher-nfd-worker-m62vv                                           1/1     Running            112 (58d ago)       93d
cattle-sriov-system        pod/sriov-rancher-nfd-worker-m8pqs                                           1/1     Running            110 (38d ago)       93d
cattle-sriov-system        pod/sriov-rancher-nfd-worker-xjz5j                                           1/1     Running            1159 (38d ago)      93d
cattle-sriov-system        pod/sriov-rancher-nfd-worker-xxnsn                                           1/1     Running            84 (58d ago)        93d
cattle-system              pod/cattle-cluster-agent-7c7847776-dpszh                                     0/1     CrashLoopBackOff   3894 (43s ago)      13d
cattle-system              pod/cattle-cluster-agent-7c7847776-jvc68                                     0/1     CrashLoopBackOff   3894 (3m14s ago)    13d
cattle-system              pod/rancher-webhook-744c69455f-t8jm9                                         1/1     Running            0                   64d
kube-system                pod/etcd-first-workload-cluster-control-plane-c26hf                          1/1     Running            28 (63d ago)        93d
kube-system                pod/etcd-first-workload-cluster-control-plane-nx6fm                          1/1     Running            9 (63d ago)         93d
kube-system                pod/etcd-first-workload-cluster-control-plane-zwqd5                          1/1     Running            47 (63d ago)        93d
kube-system                pod/kube-apiserver-first-workload-cluster-control-plane-c26hf                1/1     Running            26 (63d ago)        93d
kube-system                pod/kube-apiserver-first-workload-cluster-control-plane-nx6fm                1/1     Running            6                   93d
kube-system                pod/kube-apiserver-first-workload-cluster-control-plane-zwqd5                1/1     Running            43 (58d ago)        93d
kube-system                pod/kube-controller-manager-first-workload-cluster-control-plane-c26hf       1/1     Running            10538 (58d ago)     93d
kube-system                pod/kube-controller-manager-first-workload-cluster-control-plane-nx6fm       1/1     Running            10601 (58d ago)     93d
kube-system                pod/kube-controller-manager-first-workload-cluster-control-plane-zwqd5       1/1     Running            12588 (58d ago)     93d
kube-system                pod/kube-proxy-first-workload-cluster-control-plane-c26hf                    1/1     Running            8                   58d
kube-system                pod/kube-proxy-first-workload-cluster-control-plane-nx6fm                    1/1     Running            0                   58d
kube-system                pod/kube-proxy-first-workload-cluster-control-plane-zwqd5                    1/1     Running            2 (63d ago)         58d
kube-system                pod/kube-proxy-first-workload-cluster-worker-md-md0-76fc8bb46bx8n9bc-7kh4v   1/1     Running            1 (50d ago)         58d
kube-system                pod/kube-proxy-first-workload-cluster-worker-md-md0-76fc8bb46bx8n9bc-qf4nf   1/1     Running            0                   58d
kube-system                pod/kube-proxy-first-workload-cluster-worker-md-md0-76fc8bb46bx8n9bc-wf4bc   1/1     Running            0                   58d
kube-system                pod/kube-scheduler-first-workload-cluster-control-plane-c26hf                1/1     Running            8 (63d ago)         93d
kube-system                pod/kube-scheduler-first-workload-cluster-control-plane-nx6fm                1/1     Running            7 (63d ago)         93d
kube-system                pod/kube-scheduler-first-workload-cluster-control-plane-zwqd5                1/1     Running            6 (63d ago)         93d
kube-system                pod/multus-ds-4pg8f                                                          1/1     Running            6 (58d ago)         93d
kube-system                pod/multus-ds-fn8vs                                                          1/1     Running            5 (58d ago)         93d
kube-system                pod/multus-ds-kfjp5                                                          1/1     Running            5 (63d ago)         93d
kube-system                pod/multus-ds-nj2tc                                                          1/1     Running            5 (63d ago)         93d
kube-system                pod/multus-ds-wkscb                                                          1/1     Running            3 (63d ago)         84d
kube-system                pod/multus-ds-z28c6                                                          1/1     Running            6 (58d ago)         93d
kube-system                pod/multus-rke2-whereabouts-c2sc6                                            1/1     Running            5 (58d ago)         93d
kube-system                pod/multus-rke2-whereabouts-nn482                                            1/1     Running            6 (63d ago)         93d
kube-system                pod/multus-rke2-whereabouts-p85xn                                            1/1     Running            6 (58d ago)         93d
kube-system                pod/multus-rke2-whereabouts-pghft                                            1/1     Running            6 (58d ago)         93d
kube-system                pod/multus-rke2-whereabouts-q7vwd                                            1/1     Running            5 (63d ago)         93d
kube-system                pod/multus-rke2-whereabouts-zrs9f                                            1/1     Running            5 (63d ago)         93d
kube-system                pod/rke2-coredns-rke2-coredns-854f797f64-24kb5                               1/1     Running            0                   58d
kube-system                pod/rke2-coredns-rke2-coredns-854f797f64-w9s5x                               1/1     Running            0                   64d
kube-system                pod/rke2-coredns-rke2-coredns-autoscaler-5544889fd7-64665                    1/1     Running            0                   64d
kube-system                pod/rke2-ingress-nginx-controller-48f57                                      1/1     Running            14 (58d ago)        93d
kube-system                pod/rke2-ingress-nginx-controller-5d85c                                      1/1     Running            633 (38d ago)       93d
kube-system                pod/rke2-ingress-nginx-controller-654kg                                      1/1     Running            8 (58d ago)         93d
kube-system                pod/rke2-ingress-nginx-controller-7cvdv                                      1/1     Running            21 (58d ago)        93d
kube-system                pod/rke2-ingress-nginx-controller-7fsv6                                      1/1     Running            5 (63d ago)         93d
kube-system                pod/rke2-ingress-nginx-controller-sbl6f                                      1/1     Running            7 (58d ago)         93d
kube-system                pod/rke2-metrics-server-5d66f5dfbc-zkns6                                     1/1     Running            0                   64d
longhorn-system            pod/csi-attacher-59db6bb96c-78nfr                                            1/1     Running            0                   64d
longhorn-system            pod/csi-attacher-59db6bb96c-hcrhx                                            1/1     Running            0                   64d
longhorn-system            pod/csi-attacher-59db6bb96c-s5qsv                                            1/1     Running            0                   64d
longhorn-system            pod/csi-provisioner-6d44d79b88-lwmnk                                         1/1     Running            0                   64d
longhorn-system            pod/csi-provisioner-6d44d79b88-qp24t                                         1/1     Running            0                   64d
longhorn-system            pod/csi-provisioner-6d44d79b88-rwrcw                                         1/1     Running            1 (58d ago)         64d
longhorn-system            pod/csi-resizer-74cc7c9c95-tzf2b                                             1/1     Running            0                   64d
longhorn-system            pod/csi-resizer-74cc7c9c95-wsn5t                                             1/1     Running            0                   64d
longhorn-system            pod/csi-resizer-74cc7c9c95-zl7sn                                             1/1     Running            0                   64d
longhorn-system            pod/csi-snapshotter-7f7685dfdb-95jhs                                         1/1     Running            0                   64d
longhorn-system            pod/csi-snapshotter-7f7685dfdb-hw2vq                                         1/1     Running            0                   64d
longhorn-system            pod/csi-snapshotter-7f7685dfdb-zcsh5                                         1/1     Running            0                   64d
longhorn-system            pod/engine-image-ei-ef01bf86-7x9r2                                           1/1     Running            5 (63d ago)         93d
longhorn-system            pod/engine-image-ei-ef01bf86-bj8cm                                           1/1     Running            6 (63d ago)         93d
longhorn-system            pod/engine-image-ei-ef01bf86-c82pq                                           1/1     Running            6 (58d ago)         93d
longhorn-system            pod/engine-image-ei-ef01bf86-j79rf                                           1/1     Running            5 (58d ago)         93d
longhorn-system            pod/engine-image-ei-ef01bf86-rkfk8                                           1/1     Running            6 (58d ago)         93d
longhorn-system            pod/engine-image-ei-ef01bf86-zqrw5                                           1/1     Running            5 (63d ago)         93d
longhorn-system            pod/instance-manager-e-3e1d0c8e08523544adef621f439d244a                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-e-4519fef970ffb11b6016ea7b3ac7eb89                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-e-96efd5cd8c6551012e84ac214050370d                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-e-c4733d0f22b91d089d7c2fa61d09c73e                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-e-c713dc396a2cf1836d8e5e243cf78655                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-e-cc95452c2e15d73105ca44f7d2560a96                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-r-3e1d0c8e08523544adef621f439d244a                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-r-4519fef970ffb11b6016ea7b3ac7eb89                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-r-96efd5cd8c6551012e84ac214050370d                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-r-c4733d0f22b91d089d7c2fa61d09c73e                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-r-c713dc396a2cf1836d8e5e243cf78655                      1/1     Running            0                   58d
longhorn-system            pod/instance-manager-r-cc95452c2e15d73105ca44f7d2560a96                      1/1     Running            0                   58d
longhorn-system            pod/longhorn-admission-webhook-5bf468b585-kz24k                              1/1     Running            0                   64d
longhorn-system            pod/longhorn-admission-webhook-5bf468b585-qjz9l                              1/1     Running            0                   64d
longhorn-system            pod/longhorn-conversion-webhook-588cfff8b-dpq6q                              1/1     Running            0                   64d
longhorn-system            pod/longhorn-conversion-webhook-588cfff8b-p7ddb                              1/1     Running            0                   64d
longhorn-system            pod/longhorn-csi-plugin-7kk4d                                                3/3     Running            32 (58d ago)        93d
longhorn-system            pod/longhorn-csi-plugin-9ptdx                                                3/3     Running            41 (58d ago)        93d
longhorn-system            pod/longhorn-csi-plugin-cc482                                                3/3     Running            33 (58d ago)        93d
longhorn-system            pod/longhorn-csi-plugin-dczhd                                                3/3     Running            35 (58d ago)        93d
longhorn-system            pod/longhorn-csi-plugin-vbc88                                                3/3     Running            29 (58d ago)        93d
longhorn-system            pod/longhorn-csi-plugin-wvvk2                                                3/3     Running            37 (58d ago)        93d
longhorn-system            pod/longhorn-driver-deployer-6f6dd986dc-dj7w5                                1/1     Running            0                   64d
longhorn-system            pod/longhorn-manager-8lv7k                                                   1/1     Running            6 (58d ago)         93d
longhorn-system            pod/longhorn-manager-kkmq9                                                   1/1     Running            5 (63d ago)         93d
longhorn-system            pod/longhorn-manager-lxdf2                                                   1/1     Running            6 (58d ago)         93d
longhorn-system            pod/longhorn-manager-t6kd9                                                   1/1     Running            7 (63d ago)         93d
longhorn-system            pod/longhorn-manager-wjgk4                                                   1/1     Running            7 (63d ago)         93d
longhorn-system            pod/longhorn-manager-xzvnn                                                   1/1     Running            7 (58d ago)         93d
longhorn-system            pod/longhorn-recovery-backend-66f74fdf9-cv4rz                                1/1     Running            0                   64d
longhorn-system            pod/longhorn-recovery-backend-66f74fdf9-rtx5v                                1/1     Running            0                   64d
longhorn-system            pod/longhorn-ui-7d959687bf-2v9nb                                             1/1     Running            4 (58d ago)         64d
longhorn-system            pod/longhorn-ui-7d959687bf-r84lp                                             1/1     Running            4 (58d ago)         64d
metallb-system             pod/metallb-controller-6bb94c65c5-shbwb                                      1/1     Running            0                   64d
metallb-system             pod/metallb-speaker-9tstd                                                    1/1     Running            7 (58d ago)         93d
metallb-system             pod/metallb-speaker-fwfsp                                                    1/1     Running            8 (58d ago)         93d
metallb-system             pod/metallb-speaker-xxlkq                                                    1/1     Running            8 (58d ago)         93d
tigera-operator            pod/tigera-operator-5f94f55c97-cdg25                                         1/1     Running            2 (58d ago)         64d
udmapp                     pod/acs-pod251-0                                                             1/1     Running            0                   52d
udmapp                     pod/acs-pod251-1                                                             1/1     Running            0                   52d
udmapp                     pod/acs-pod81-0                                                              1/1     Running            0                   38d
udmapp                     pod/acs-pod81-1                                                              1/1     Running            0                   38d
udmapp                     pod/appctrlservice81-84b55976f5-8n5lp                                        2/2     Running            0                   38d
udmapp                     pod/appctrlservice81-84b55976f5-vj9zk                                        2/2     Running            0                   38d
udmapp                     pod/auditlog-76d4d9ff9b-5v67p                                                1/1     Running            0                   52d
udmapp                     pod/auditlog-76d4d9ff9b-cjqjh                                                1/1     Running            0                   52d
udmapp                     pod/backupmgr-0                                                              1/1     Running            3 (50d ago)         52d
udmapp                     pod/backupmgr-1                                                              1/1     Running            0                   52d
udmapp                     pod/certmgr-76649b7845-22j6r                                                 1/1     Running            0                   52d
udmapp                     pod/certmgr-76649b7845-nll2l                                                 1/1     Running            0                   52d
udmapp                     pod/cgplite-0                                                                1/1     Running            0                   52d
udmapp                     pod/cgplite-1                                                                1/1     Running            0                   52d
udmapp                     pod/cmcenter-585b894d8f-lwfqf                                                1/1     Running            0                   52d
udmapp                     pod/cmcenter-585b894d8f-sx6rg                                                1/1     Running            0                   52d
udmapp                     pod/cmdproxy-76dd49c6cb-ghm6m                                                1/1     Running            0                   52d
udmapp                     pod/cmdproxy-76dd49c6cb-t772f                                                1/1     Running            7 (38d ago)         52d
udmapp                     pod/cmf-pod251-0                                                             2/2     Running            0                   52d
udmapp                     pod/cmf-pod251-1                                                             2/2     Running            0                   52d
udmapp                     pod/cmf-pod251-2                                                             2/2     Running            0                   52d
udmapp                     pod/cmf-pod81-0                                                              2/2     Running            0                   38d
udmapp                     pod/cmf-pod81-1                                                              2/2     Running            0                   38d
udmapp                     pod/cmf-pod81-2                                                              2/2     Running            0                   38d
udmapp                     pod/collectandcheck-0                                                        1/1     Running            0                   52d
udmapp                     pod/collectandcheck-1                                                        1/1     Running            5 (38d ago)         52d
udmapp                     pod/cse-etcd-0                                                               1/1     Running            0                   52d
udmapp                     pod/cse-etcd-1                                                               1/1     Running            0                   52d
udmapp                     pod/cse-etcd-2                                                               1/1     Running            0                   52d
udmapp                     pod/cse-service-center-6f59cbc74f-2ttjv                                      1/1     Running            0                   52d
udmapp                     pod/cse-service-center-6f59cbc74f-469qn                                      1/1     Running            0                   52d
udmapp                     pod/cse-service-center-6f59cbc74f-67qjj                                      1/1     Running            0                   52d
udmapp                     pod/cslbip-pod251-0                                                          3/3     Running            0                   41d
udmapp                     pod/cslbip-pod251-1                                                          3/3     Running            0                   52d
udmapp                     pod/cslbip-pod81-0                                                           3/3     Running            0                   38d
udmapp                     pod/cslbip-pod81-1                                                           3/3     Running            0                   38d
udmapp                     pod/cslbipom-pod251-0                                                        3/3     Running            1 (42d ago)         52d
udmapp                     pod/cslbipom-pod251-1                                                        3/3     Running            0                   52d
udmapp                     pod/cslbipom-pod81-0                                                         3/3     Running            0                   38d
udmapp                     pod/cslbipom-pod81-1                                                         3/3     Running            0                   38d
udmapp                     pod/cspagentmanager-6c64969b65-7ctg7                                         1/1     Running            0                   52d
udmapp                     pod/cspagentmanager-6c64969b65-wb5sc                                         1/1     Running            0                   52d
udmapp                     pod/csphaservice-66d85bf87c-45knb                                            1/1     Running            0                   52d
udmapp                     pod/csphaservice-66d85bf87c-fshwq                                            1/1     Running            23 (38d ago)        52d
udmapp                     pod/csphaservice-66d85bf87c-tkp74                                            1/1     Running            0                   52d
udmapp                     pod/cspmtcenter-8db676958-nqvdc                                              1/1     Running            1 (50d ago)         52d
udmapp                     pod/cspmtcenter-8db676958-rwx9n                                              1/1     Running            0                   52d
udmapp                     pod/cspnetconfgw-0                                                           1/1     Running            0                   52d
udmapp                     pod/cspnetconfgw-1                                                           1/1     Running            0                   52d
udmapp                     pod/cspopsagent-fddjg                                                        1/1     Running            0                   52d
udmapp                     pod/cspopsagent-gjgnq                                                        1/1     Running            0                   52d
udmapp                     pod/cspopsagent-k44wf                                                        1/1     Running            0                   52d
udmapp                     pod/cspopsagent-kbbrv                                                        1/1     Running            0                   52d
udmapp                     pod/cspopsagent-qg98f                                                        1/1     Running            1 (50d ago)         52d
udmapp                     pod/cspopsagent-qrnwl                                                        1/1     Running            0                   52d
udmapp                     pod/csptzm-579576d5b9-d8xg5                                                  1/1     Running            0                   52d
udmapp                     pod/csptzm-579576d5b9-g48kw                                                  1/1     Running            0                   52d
udmapp                     pod/dbbroker-pod-251-0                                                       2/2     Running            0                   52d
udmapp                     pod/dbbroker-pod-251-1                                                       2/2     Running            0                   52d
udmapp                     pod/ddbsf-pod251-68b44cc6d5-7hxm7                                            2/2     Running            0                   52d
udmapp                     pod/ddbsf-pod251-68b44cc6d5-f9lrl                                            2/2     Running            0                   52d
udmapp                     pod/ddbsf-pod251-68b44cc6d5-gnf9j                                            2/2     Running            0                   52d
udmapp                     pod/ddbsf-pod81-5f98cb5ddd-2v5r2                                             2/2     Running            0                   38d
udmapp                     pod/ddbsf-pod81-5f98cb5ddd-bqsc8                                             2/2     Running            0                   38d
udmapp                     pod/ddbsf-pod81-5f98cb5ddd-dgrnj                                             2/2     Running            0                   38d
udmapp                     pod/febs-77c9c4b8b6-q2kck                                                    1/1     Running            0                   52d
udmapp                     pod/febs-77c9c4b8b6-rvc6p                                                    1/1     Running            0                   52d
udmapp                     pod/fileserver-0                                                             1/1     Running            1 (49d ago)         52d
udmapp                     pod/fileserver-1                                                             1/1     Running            0                   52d
udmapp                     pod/filetransfer-0                                                           1/1     Running            0                   52d
udmapp                     pod/filetransfer-1                                                           1/1     Running            6 (38d ago)         52d
udmapp                     pod/fmservice-5c4868b5b6-gq2xt                                               1/1     Running            0                   52d
udmapp                     pod/fmservice-5c4868b5b6-qp6wc                                               1/1     Running            3 (38d ago)         52d
udmapp                     pod/gaussdb-0                                                                1/1     Running            6 (38d ago)         52d
udmapp                     pod/gaussdb-1                                                                1/1     Running            0                   52d
udmapp                     pod/hafetcd-pod251-0                                                         1/1     Running            0                   52d
udmapp                     pod/hafetcd-pod251-1                                                         1/1     Running            0                   52d
udmapp                     pod/hafetcd-pod251-2                                                         1/1     Running            0                   52d
udmapp                     pod/hafetcd-pod81-0                                                          1/1     Running            0                   38d
udmapp                     pod/hafetcd-pod81-1                                                          1/1     Running            0                   38d
udmapp                     pod/hafetcd-pod81-2                                                          1/1     Running            0                   38d
udmapp                     pod/helpcenter-0                                                             1/1     Running            0                   52d
udmapp                     pod/helpcenter-1                                                             1/1     Running            5 (38d ago)         52d
udmapp                     pod/hwipam-26lzx                                                             2/2     Running            0                   52d
udmapp                     pod/hwipam-2z8w7                                                             2/2     Running            0                   52d
udmapp                     pod/hwipam-fx2lb                                                             2/2     Running            0                   52d
udmapp                     pod/hwipam-hsbm6                                                             2/2     Running            0                   52d
udmapp                     pod/hwipam-nj49c                                                             2/2     Running            0                   52d
udmapp                     pod/hwipam-rcbtb                                                             2/2     Running            0                   52d
udmapp                     pod/hwipamctrl-6f9479c794-4sdlc                                              2/2     Running            0                   52d
udmapp                     pod/hwipamctrl-6f9479c794-kr4pk                                              2/2     Running            0                   52d
udmapp                     pod/ipctrl-pod251-0                                                          2/2     Running            0                   52d
udmapp                     pod/ipctrl-pod251-1                                                          2/2     Running            0                   52d
udmapp                     pod/ipctrl-pod251-2                                                          2/2     Running            0                   52d
udmapp                     pod/ipctrl-pod81-0                                                           2/2     Running            0                   38d
udmapp                     pod/ipctrl-pod81-1                                                           2/2     Running            0                   38d
udmapp                     pod/ipctrl-pod81-2                                                           2/2     Running            0                   38d
udmapp                     pod/kafka-0                                                                  1/1     Running            0                   52d
udmapp                     pod/kafka-1                                                                  1/1     Running            6 (49d ago)         52d
udmapp                     pod/license-787d876c6f-2xxpj                                                 1/1     Running            8 (38d ago)         52d
udmapp                     pod/license-787d876c6f-cq6fm                                                 1/1     Running            0                   52d
udmapp                     pod/messagetrace-0                                                           1/1     Running            22 (38d ago)        52d
udmapp                     pod/messagetrace-1                                                           1/1     Running            0                   52d
udmapp                     pod/mmlservice-0                                                             1/1     Running            0                   52d
udmapp                     pod/mmlservice-1                                                             1/1     Running            20 (38d ago)        52d
udmapp                     pod/modulekeeper-0                                                           1/1     Running            0                   36d
udmapp                     pod/modulekeeper-1                                                           1/1     Running            0                   36d
udmapp                     pod/omb-pod251-78465f8474-942nv                                              2/2     Running            0                   52d
udmapp                     pod/omb-pod251-78465f8474-pvfct                                              2/2     Running            0                   52d
udmapp                     pod/omb-pod81-7f989ff79b-52dj7                                               2/2     Running            0                   38d
udmapp                     pod/omb-pod81-7f989ff79b-xh588                                               2/2     Running            0                   38d
udmapp                     pod/ombroker251-0                                                            1/1     Running            0                   52d
udmapp                     pod/ombroker251-1                                                            1/1     Running            0                   52d
udmapp                     pod/ombroker81-0                                                             1/1     Running            0                   38d
udmapp                     pod/ombroker81-1                                                             1/1     Running            0                   38d
udmapp                     pod/omcache-0                                                                1/1     Running            1 (49d ago)         52d
udmapp                     pod/omcache-1                                                                1/1     Running            0                   52d
udmapp                     pod/omlb-0                                                                   0/1     Running            13901 (4m51s ago)   52d
udmapp                     pod/omlb-1                                                                   1/1     Running            0                   36d
udmapp                     pod/patchmgr-6bdb8fd4c-6249w                                                 1/1     Running            0                   52d
udmapp                     pod/patchmgr-6bdb8fd4c-bqzb5                                                 1/1     Running            0                   52d
udmapp                     pod/pmscalc-69db9fdd79-8vwlg                                                 1/1     Running            0                   52d
udmapp                     pod/pmscalc-69db9fdd79-t4t2b                                                 1/1     Running            0                   52d
udmapp                     pod/pmsmgr-54b44c6775-9884c                                                  1/1     Running            0                   52d
udmapp                     pod/pmsmgr-54b44c6775-mkw6h                                                  1/1     Running            0                   52d
udmapp                     pod/privilege-6b77c6bfcd-2lcxc                                               1/1     Running            0                   52d
udmapp                     pod/privilege-6b77c6bfcd-wcm29                                               1/1     Running            5 (38d ago)         52d
udmapp                     pod/runlog-0                                                                 1/1     Running            5 (3d20h ago)       52d
udmapp                     pod/runlog-1                                                                 1/1     Running            4 (7d15h ago)       52d
udmapp                     pod/servicegovernance-786f6b498c-8lnwl                                       1/1     Running            0                   52d
udmapp                     pod/servicegovernance-786f6b498c-pvdv8                                       1/1     Running            0                   52d
udmapp                     pod/sfm-pod251-59477bdcd6-7cvlp                                              12/12   Running            0                   41d
udmapp                     pod/sfm-pod251-59477bdcd6-lmhmw                                              12/12   Running            3 (41d ago)         42d
udmapp                     pod/sfm-pod81-845bfdf98c-2mldk                                               10/10   Running            3 (29d ago)         38d
udmapp                     pod/sfm-pod81-845bfdf98c-wmrcc                                               10/10   Running            0                   38d
udmapp                     pod/snmp-77b47f7bb6-bhnxq                                                    1/1     Running            0                   52d
udmapp                     pod/snmp-77b47f7bb6-n6srz                                                    1/1     Running            0                   52d
udmapp                     pod/soap-785c78b678-cs29z                                                    1/1     Running            0                   52d
udmapp                     pod/soap-785c78b678-wpb5l                                                    1/1     Running            0                   52d
udmapp                     pod/streamcollectservice-8446dd775f-8c6nr                                    1/1     Running            1 (50d ago)         52d
udmapp                     pod/streamcollectservice-8446dd775f-zvjqn                                    1/1     Running            0                   52d
udmapp                     pod/udm-uccu-pod-251-0                                                       4/4     Running            0                   42d
udmapp                     pod/udm-uccu-pod-251-1                                                       4/4     Running            0                   38d
udmapp                     pod/udm-uisu-a-pod-251-0                                                     3/3     Running            0                   2d1h
udmapp                     pod/udm-uisu-b-pod-251-0                                                     3/3     Running            0                   38d
udmapp                     pod/udmctrl-pod251-86bccd5568-k5jjt                                          4/4     Running            0                   50d
udmapp                     pod/udmctrl-pod251-86bccd5568-r96zw                                          4/4     Running            0                   52d
udmapp                     pod/upgtool-app-0                                                            1/1     Running            6 (38d ago)         52d
udmapp                     pod/upgtool-app-1                                                            1/1     Running            0                   52d
udmapp                     pod/uspid3aservice-81-0                                                      2/2     Running            0                   38d
udmapp                     pod/uspid3bservice-81-0                                                      2/2     Running            0                   38d
udmapp                     pod/usrsufaservice-81-0                                                      2/2     Running            0                   38d
udmapp                     pod/usrsufbservice-81-0                                                      2/2     Running            0                   38d
udmapp                     pod/usrsuoaservice-81-0                                                      2/2     Running            0                   38d
udmapp                     pod/usrsuobservice-81-0                                                      2/2     Running            0                   38d
udmapp                     pod/vha-pod251-0                                                             1/1     Running            0                   52d
udmapp                     pod/vha-pod251-1                                                             1/1     Running            0                   52d
udmapp                     pod/vha-pod251-2                                                             1/1     Running            0                   52d
udmapp                     pod/vha-pod81-0                                                              1/1     Running            0                   38d
udmapp                     pod/vha-pod81-1                                                              1/1     Running            0                   38d
udmapp                     pod/vha-pod81-2                                                              1/1     Running            0                   38d
udmapp                     pod/vnfmadaptor-8755cf674-428jw                                              1/1     Running            0                   52d
udmapp                     pod/vnfmadaptor-8755cf674-rh7ff                                              1/1     Running            0                   52d
udmapp                     pod/vnfpom-pod251-0                                                          1/1     Running            0                   52d
udmapp                     pod/vnfpom-pod251-1                                                          1/1     Running            0                   52d
udmapp                     pod/vnfpom-pod81-0                                                           1/1     Running            0                   38d
udmapp                     pod/vnfpom-pod81-1                                                           1/1     Running            0                   38d
udmapp                     pod/zookeeper-0                                                              1/1     Running            7 (38d ago)         52d
udmapp                     pod/zookeeper-1                                                              1/1     Running            0                   52d
udmapp                     pod/zookeeper-2                                                              1/1     Running            0                   52d
```

### Evidence of Workload Basic Functional Testing 

#### Functional tests

|Test|Description|Result|
|-|-|-|
|HLR_Location_Management_CS|Mobility Management - First-time LU of MS in VLR - The purpose of this test is to perform a successful registration of the MS first time LU in the VLR - Reference: 3GPP TS 29.002 section 8.1.2, 19.1.1|PASSED|
|HLR_Location_Management_PS|Mobility Management - First-time LU of MS in SGSN - The purpose of this test is to perform a successful registration of the MS first time LU in the SGSN - Reference: 3GPP TS 29.002 section 8.1.2, 19.1.1|PASSED|
|EPC HSS Service S6a|HSS successfully sending subscriber data to the MME during the update location for a�defined subscriber - Verify that the HSS can successfully send the subscriber data to the MME during the�Update Location for a subscriber who is provided with the EPS service.|PASSED|
|EPC HSS Service S6a|eUTRAN authentication information retrieval - Verify that the Authentication Information Retrieval Procedure can be triggered by the MME to request authentication information from the HSS.|PASSED|
|EPC HSS Service S6a|Triggering the NOR procedure after the MME allocates a PDN GW IP address for a subscriber - Verify that the DSU procedure can be triggered when the subscriber data is deleted.|PASSED|
|IMS HSS Service (Cx)|Location Management Procedure - Re-registration - The purpose of this test is to verify the UE can re-register successful|PASSED|
|IMS HSS Service (Cx)|Conversation setup process - Called user is registered - The purpose of this test is to verify that if the called subscriber is registered in HSS, HSS can send the subscriber�s current S-CSCF name to I-CSCF by LIA message.|PASSED|
|IMS HSS Service (Cx)|IMS Multimedia Authentication Request - Test IMS Multimedia Authentication Request|PASSED|
|IMS HSS Service (Cx)|Location Management Procedure - Registration - The purpose of this test is to verify the UE can register successful|PASSED|
|UDM|UE Context Management (Nudm_UECM): AMF registration / deregistration - SMF registration�/�deregistration - SMSF registration�/�deregistration - UDM initiate NF��deregistration|PASSED|
|UDM|Subscriber Data Management (Nudm_SDM): Access and Mobility Subscription Data Retrieval - Session Management Subscription Data Retrieval - Subscription /�Unsubscribe�to notifications of data change - Data Change Notification To NF|PASSED|
|UDM|Event Exposure (Nudm_EE): Subscription to Notification of event occurrence - Event Occurrence Notification|PASSED|
|AUSF|AUSF Authenticate (Nausf_AUTH): 5G-AKA authentication - EAP-AKA Prime authentication|PASSED|

- Evidences: **pcapng traces available**

#### Performance tests

|Test|Description|Result|
|-|-|-|
|HLR Performances|HLR (MAP) Performances in not mixed traffic|PASSED|
|HSS Performances|HSS (Cx&S6a) Performances in not mixed traffic|PASSED|
|HLR Performances not mixed traffic limits|HLR (MAP) Performances in not mixed traffic limits (individual shoot of 4800 msg/s for each type of request on 10k active subscribers)|PASSED|
|HSS Performances not mixed traffic limits|HSS (Cx&S6a) Performances in not mixed traffic limits (individual shoot of 4800 msg/s for each type of request on 10k active subscribers)|PASSED|
|HLR/HSS Performances mixed traffic|HLR/HSS (MAP, Cx & S6a) Performances in mixed traffic|PASSED|
