# Validation Report for Casa Systems NRF v8.M8.0

- Date: **06 - February - 2024**

- Validation Responsible:  **Guillermo Carreto (Telefónica)**

- Validation Team: **Guillermo Carreto (Telefónica) and David Carrera (Nearby Computing)**

- Result: **PASSED**

- Comments:

## Validated Workload

- Provider of CNF: **Casa Systems**

- Name of CNF: **Network Repository Function (NRF)**

- Version of CNF: **v8.M8.0**

- Platform capabilities required: < mark an X where applicable >
  
  * [ ] CPU pinning
  * [ ] HugePages
  * [ ] Multus CNI
  * [ ] SR-IOV
  * [ ] PTP
  * [x] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI

## Validation Platform

- Sylva version: **v0.3.1**

- Validation environment used: **TEF-1**

- Flavour used for validation:
  
  * [ ] BareMetal + Vanilla K8s
  * [ ] BareMetal + RKE2
  * [ ] OpenStack + Vanilla K8s
  * [x] OpenStack + RKE2
  * [ ] VMware stack + Vanilla K8s
  * [ ] VMware stack + RKE2

- HW used for the platform (servers):

  - **4 x HPE ProLiant DL380 Gen10 (72 vCPUs, 384GB RAM, 878GB Local Storage)**
  - **2 x HPE ProLiant DL380 Gen10 (72 vCPUs, 576GB RAM, 731GB Local Storage)**
  - **2 x Intel R2208WFTZSR (96 vCPUs, 192GB RAM, 438GB Local Storage)**

- Virtual Machines used for the platform (only applies if a Sylva's IaaS flavour like OpenStack or VMware has been used):
  
  - **CAPI Management Cluster --> 3 x VM: 4 vCPUs, 8GB RAM, 80GB Local Storage**
  - **CAPI Workload Cluster --> 1 x VM: 16 vCPUs, 32GB RAM, 200GB Local Storage**

## Validation Evidences

### Evidence of Platform Configuration

The following is a screenshot of the evidence that the platform was configured with the capabilities required by the workload under validation (in this case, only Calico CNI is required):

```bash
2024-02-06 10:28:34,953 - xtesting.ci.run_tests - INFO - Xtesting report:

+-------------------------------------------+------------------------------+--------------------------------------------------------+------------------+----------------+
|                 TEST CASE                 |           PROJECT            |                          TIER                          |     DURATION     |     RESULT     |
+-------------------------------------------+------------------------------+--------------------------------------------------------+------------------+----------------+
|          Multus CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|          Multus NAD CRD Creation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:07       |      FAIL      |
|          SR-IOV CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|     SR-IOV Device Plugin Installation     |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|        WhereAbouts CNI Installation       |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|          Calico CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      PASS      |
|          Flannel CNI Installation         |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|          Cilium CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|         WeaveNet CNI Installation         |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
+-------------------------------------------+------------------------------+--------------------------------------------------------+------------------+----------------+
```

### Evidence of Workload Deployment

Next evidence includes all the objects created due to Casa Systems Operators and Casa Systems NRF deployment:

```bash
ubuntu@workload-cluster-nearby-cp-b79c13e277-ltsfp:~$ kubectl get all -n casa
NAME                                                                  READY   STATUS      RESTARTS   AGE
pod/1a602f81001e1ed0c80c6ac1d9fad2788686bb06f3b36151cd51a2e842b249c   0/1     Completed   0          54m
pod/335422ad822723106303c80cf2e8bd749f140e319e87ddb1becb714f07gmw2s   0/1     Completed   0          54m
pod/4485301ff2ed0db03b76f107beb5cf870f62b2a29351fe46d0c03b5c44wgvts   0/1     Completed   0          54m
pod/6b420aae4f9c2412770b610fcd91e3cce96296a0cceb7c8b503bb45807czb2q   0/1     Completed   0          56m
pod/a857d46c0b90b0cc76ae7f6a32ff6415fabb298684a9e560d68c319c5fnxdl4   0/1     Completed   0          54m
pod/axyom-apiserver-5c5987597-xl54b                                   1/1     Running     0          55m
pod/axyom-controller-manager-7d89b5c746-xkwtx                         2/2     Running     0          53m
pod/axyom-etcd-0                                                      1/1     Running     0          55m
pod/axyom-smf-controller-manager-bbbc85956-pnqhv                      2/2     Running     0          54m
pod/axyom-upf-controller-manager-8dd97b56d-t6tsj                      2/2     Running     0          54m
pod/casa-axyom-dashboard-65fb6ffccb-xrlps                             1/1     Running     0          55m
pod/casa-operators-s9bnm                                              1/1     Running     0          55m
pod/casa-sa-watcher-patcher-wdnrq                                     0/1     Completed   0          57m
pod/casa-secret-creator-gk5wh                                         0/1     Completed   0          57m
pod/casa-smf-operators-s68g4                                          1/1     Running     0          55m
pod/casa-upf-operators-k4xjl                                          1/1     Running     0          55m
pod/dc3e277bc48f4ba981246a88dd9a24d36c78230894f1a24ed06528c39989ntj   0/1     Completed   0          54m
pod/nrf1-nrf-54c87b7b57-8p665                                         2/2     Running     0          53m
pod/nrf1-nrf-bootstrapping-744d88dc54-qpk4w                           2/2     Running     0          52m
pod/nrf1-nrf-timer-8b6965dc4-gjftm                                    2/2     Running     0          52m
pod/redis-sentinel-operator-catalog-x7xl8                             1/1     Running     0          57m
pod/redisoperator-ddf449f8d-rjc69                                     1/1     Running     0          56m
pod/rfr-nrf1-sentinel-redis-0                                         2/2     Running     0          53m
pod/rfs-nrf1-sentinel-redis-7c9db6f9bb-7mltb                          2/2     Running     0          53m
pod/secret-updater-28452480-8v5zl                                     0/1     Completed   0          4m11s

NAME                                                   TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)           AGE
service/axyom-controller-manager-metrics-service       ClusterIP   10.43.154.231   <none>        8443/TCP          53m
service/axyom-controller-manager-service               ClusterIP   10.43.120.48    <none>        443/TCP           53m
service/axyom-dashboard                                ClusterIP   10.43.112.107   <none>        80/TCP            55m
service/axyom-mgmt                                     ClusterIP   10.43.9.248     <none>        443/TCP           55m
service/axyom-smf-controller-manager-metrics-service   ClusterIP   10.43.49.52     <none>        8443/TCP          54m
service/axyom-smf-controller-manager-service           ClusterIP   10.43.1.89      <none>        443/TCP           54m
service/axyom-smf-webhook-service                      ClusterIP   10.43.201.48    <none>        443/TCP           54m
service/axyom-upf-controller-manager-metrics-service   ClusterIP   10.43.167.208   <none>        8443/TCP          54m
service/axyom-upf-controller-manager-service           ClusterIP   10.43.175.109   <none>        443/TCP           54m
service/axyom-upf-webhook-service                      ClusterIP   10.43.54.218    <none>        443/TCP           54m
service/axyom-webhook-service                          ClusterIP   10.43.146.14    <none>        443/TCP           53m
service/casa-operators                                 ClusterIP   10.43.232.0     <none>        50051/TCP         55m
service/casa-smf-operators                             ClusterIP   10.43.134.123   <none>        50051/TCP         55m
service/casa-upf-operators                             ClusterIP   10.43.182.120   <none>        50051/TCP         55m
service/etcd-svc                                       ClusterIP   10.43.177.88    <none>        2379/TCP          55m
service/nrf1-nnrf                                      ClusterIP   10.43.158.93    <none>        80/TCP            53m
service/nrf1-nnrf-bootstrapping                        ClusterIP   10.43.158.118   <none>        80/TCP            53m
service/nrf1-nrf                                       ClusterIP   10.43.108.250   <none>        80/TCP,8081/TCP   53m
service/nrf1-nrf-bootstrapping                         ClusterIP   10.43.9.56      <none>        80/TCP            52m
service/nrf1-nrf-timer                                 ClusterIP   10.43.161.147   <none>        80/TCP            52m
service/redis-sentinel-operator-catalog                ClusterIP   10.43.203.166   <none>        50051/TCP         57m
service/rfg-nrf1-sentinel-redis                        ClusterIP   10.43.132.237   <none>        56379/TCP         53m
service/rfg-nrf1-sentinel-redis-local-client           ClusterIP   10.43.163.211   <none>        56379/TCP         53m
service/rfr-nrf1-sentinel-redis                        ClusterIP   None            <none>        9121/TCP          53m
service/rfs-nrf1-sentinel-redis                        ClusterIP   10.43.62.51     <none>        27349/TCP         53m

NAME                                           READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/axyom-apiserver                1/1     1            1           55m
deployment.apps/axyom-controller-manager       1/1     1            1           53m
deployment.apps/axyom-smf-controller-manager   1/1     1            1           54m
deployment.apps/axyom-upf-controller-manager   1/1     1            1           54m
deployment.apps/casa-axyom-dashboard           1/1     1            1           55m
deployment.apps/nrf1-nrf                       1/1     1            1           53m
deployment.apps/nrf1-nrf-bootstrapping         1/1     1            1           52m
deployment.apps/nrf1-nrf-timer                 1/1     1            1           52m
deployment.apps/redisoperator                  1/1     1            1           56m
deployment.apps/rfs-nrf1-sentinel-redis        1/1     1            1           53m

NAME                                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/axyom-apiserver-5c5987597                1         1         1       55m
replicaset.apps/axyom-controller-manager-7d89b5c746      1         1         1       53m
replicaset.apps/axyom-smf-controller-manager-bbbc85956   1         1         1       54m
replicaset.apps/axyom-upf-controller-manager-8dd97b56d   1         1         1       54m
replicaset.apps/casa-axyom-dashboard-65fb6ffccb          1         1         1       55m
replicaset.apps/nrf1-nrf-54c87b7b57                      1         1         1       53m
replicaset.apps/nrf1-nrf-bootstrapping-744d88dc54        1         1         1       52m
replicaset.apps/nrf1-nrf-timer-8b6965dc4                 1         1         1       52m
replicaset.apps/redisoperator-ddf449f8d                  1         1         1       56m
replicaset.apps/rfs-nrf1-sentinel-redis-7c9db6f9bb       1         1         1       53m

NAME                                       READY   AGE
statefulset.apps/axyom-etcd                1/1     55m
statefulset.apps/rfr-nrf1-sentinel-redis   1/1     53m

NAME                           SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/secret-updater   0 * * * *   False     0        4m11s           57m

NAME                                                                        COMPLETIONS   DURATION   AGE
job.batch/1a602f81001e1ed0c80c6ac1d9fad2788686bb06f3b36151cd51a2e842ed3c6   1/1           10s        54m
job.batch/335422ad822723106303c80cf2e8bd749f140e319e87ddb1becb714f07c97db   1/1           8s         54m
job.batch/4485301ff2ed0db03b76f107beb5cf870f62b2a29351fe46d0c03b5c44c8fef   1/1           8s         54m
job.batch/6b420aae4f9c2412770b610fcd91e3cce96296a0cceb7c8b503bb458074aac1   1/1           7s         56m
job.batch/a857d46c0b90b0cc76ae7f6a32ff6415fabb298684a9e560d68c319c5f4ec1a   1/1           10s        54m
job.batch/casa-sa-watcher-patcher                                           1/1           93s        57m
job.batch/casa-secret-creator                                               1/1           6s         57m
job.batch/dc3e277bc48f4ba981246a88dd9a24d36c78230894f1a24ed06528c39946d92   1/1           8s         54m
job.batch/secret-updater-28452480                                           1/1           5s         4m11s
```

### Evidence of Workload Basic Functional Testing

#### Functional test 1

- Title: Register SMF on NRF
- Description: In this test, Casa Systems Session Management Function (SMF) CNF is registered on NRF.
- Results expected: Check that SMF is correctly registered on NRF.
- Evidences: the following command output demonstrates that this functional test was successfully run.

```bash
ubuntu@workload-cluster-nearby-cp-b79c13e277-ltsfp:~$ kubectl get nfregistration -n casa
NRF    NFINSTANCEID                           NFTYPE   NFSTATUS
nrf1   c1965de7-ebe3-4f2c-b485-ffdf674b2916   SMF      REGISTERED
```
