# Validation Report for Ericsson Signaling Controller (SC) v1.12

- Date: **12 - February - 2024**

- Validation Responsible:  **Vincent Catros (Orange)**

- Validation Team:
  * **Vincent Catros (Orange)**.
  * **Maruja Reyes Fuchs (Orange)**.

- Result: **PASSED**

- Comments: **N/A**

## Validated Workload

- Provider of CNF:  **Ericsson**

- Name of CNF: **Signaling Controller (SC)**

- Version of CNF: **v1.12**

- Platform capabilities required:
  
  * [ ] CPU pinning
  * [ ] HugePages
  * [ ] Multus CNI
  * [ ] SR-IOV
  * [ ] PTP
  * [ ] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI
  * [x] MetalLB

## Validation Platform

- Sylva version: **0.3.0**

- Validation environment code: **ORA-1**

- Flavour used for validation:
  
  * [ ] BareMetal + Vanilla K8s
  * [x] BareMetal + RKE2
  * [ ] OpenStack + Vanilla K8s
  * [ ] OpenStack + RKE2
  * [ ] VMware stack + Vanilla K8s
  * [ ] VMware stack + RKE2

- Hardware used for the platform (servers):

  - SERVER 1: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 2: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 3: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 4: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 5: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS

## Validation Evidences

### Evidence of management cluster deployment
```
ubuntu@bootstrap:~/sylva-core$ kubectl get pods -A
NAMESPACE                   NAME                                                             READY   STATUS             RESTARTS           AGE
calico-system               calico-kube-controllers-5bc6bbb6f6-zk6nc                         1/1     Running            5 (6d16h ago)      81d
calico-system               calico-node-dw26k                                                1/1     Running            0                  81d
calico-system               calico-node-p9frg                                                1/1     Running            0                  81d
calico-system               calico-node-vs8rn                                                1/1     Running            0                  81d
calico-system               calico-node-x88w7                                                1/1     Running            0                  81d
calico-system               calico-typha-7f7fd6b6c7-c55bm                                    1/1     Running            0                  81d
calico-system               calico-typha-7f7fd6b6c7-td989                                    1/1     Running            0                  81d
capi-rancher-import         capi-rancher-import-6d4587fcfb-bx6r4                             1/1     Running            0                  81d
capi-system                 capi-controller-manager-55c49b8754-bgjsf                         1/1     Running            51 (5d22h ago)     81d
capm3-system                capm3-controller-manager-6976d79599-xmjb4                        1/1     Running            126 (81d ago)      81d
capm3-system                ipam-controller-manager-79499584f5-8tnzh                         1/1     Running            0                  81d
capo-system                 capo-controller-manager-9d4f4c678-nn25m                          1/1     Running            41 (5d22h ago)     81d
cattle-fleet-local-system   fleet-agent-545c88bf5f-s5dns                                     0/1     ErrImagePull       0                  6d15h
cattle-fleet-system         fleet-controller-7695fbc6d-6kvbq                                 1/1     Running            4 (6d16h ago)      81d
cattle-fleet-system         gitjob-5cff784d68-7tzz8                                          1/1     Running            3 (6d16h ago)      81d
cattle-monitoring-system    alertmanager-rancher-monitoring-alertmanager-0                   2/2     Running            1 (81d ago)        81d
cattle-monitoring-system    prometheus-rancher-monitoring-prometheus-0                       2/3     CrashLoopBackOff   20791 (3m5s ago)   81d
cattle-monitoring-system    rancher-monitoring-grafana-768dbccd5d-t8vwh                      4/4     Running            0                  81d
cattle-monitoring-system    rancher-monitoring-kube-state-metrics-56b4477cc-4bxbt            1/1     Running            0                  81d
cattle-monitoring-system    rancher-monitoring-operator-c66c76fd9-wd59x                      1/1     Running            0                  81d
cattle-monitoring-system    rancher-monitoring-prometheus-adapter-7494f789f6-bxx2t           1/1     Running            0                  81d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-86tfm                1/1     Running            0                  81d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-krhgn                1/1     Running            0                  81d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-n5pnz                1/1     Running            0                  81d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-zv2cs                1/1     Running            0                  81d
cattle-system               check-rancher-clusters-job-cattle-system-bk672                   0/1     Completed          0                  80d
cattle-system               rancher-68865ff798-nntjv                                         1/1     Running            1 (6d16h ago)      81d
cattle-system               rancher-68865ff798-x6262                                         1/1     Running            1 (15d ago)        81d
cattle-system               rancher-68865ff798-xpmgw                                         1/1     Running            3 (6d16h ago)      81d
cattle-system               rancher-webhook-58d68fb97d-kqjmf                                 1/1     Running            0                  81d
cert-manager                cert-manager-6f799f7ff8-jxftv                                    1/1     Running            1 (20d ago)        81d
cert-manager                cert-manager-cainjector-dd668c49f-5x674                          1/1     Running            3 (6d16h ago)      81d
cert-manager                cert-manager-webhook-647dcbbdbc-2dpzg                            1/1     Running            0                  81d
cinder-csi                  openstack-cinder-csi-controllerplugin-6c6bd75db4-wt4s4           6/6     Running            120 (5d22h ago)    81d
cinder-csi                  openstack-cinder-csi-nodeplugin-57pbm                            3/3     Running            0                  81d
cinder-csi                  openstack-cinder-csi-nodeplugin-7lmgt                            3/3     Running            0                  81d
cinder-csi                  openstack-cinder-csi-nodeplugin-ks8md                            3/3     Running            1 (6d16h ago)      81d
cinder-csi                  openstack-cinder-csi-nodeplugin-nzmqv                            3/3     Running            1 (6d16h ago)      81d
cis-operator-system         cis-operator-6dfb7b656d-c2p84                                    1/1     Running            0                  81d
external-secrets            external-secrets-operator-85778f4b6d-6g62h                       1/1     Running            0                  81d
external-secrets            external-secrets-operator-cert-controller-5fb8557d-lrm7h         1/1     Running            0                  81d
external-secrets            external-secrets-operator-webhook-6fd5f846b9-x78fs               1/1     Running            0                  81d
flux-system                 flux-webui-weave-gitops-6c79db5c58-v65h4                         1/1     Running            0                  81d
flux-system                 helm-controller-74fdd6769-dft8n                                  1/1     Running            1 (20d ago)        81d
flux-system                 kustomize-controller-758c576965-5cdl6                            1/1     Running            3 (6d16h ago)      81d
flux-system                 notification-controller-58cf49bb5d-t2qt5                         1/1     Running            2 (6d16h ago)      81d
flux-system                 source-controller-7bf6d9655d-dtcrd                               1/1     Running            2 (6d16h ago)      81d
heat-operator-system        heat-operator-controller-manager-9b979f656-j29q8                 2/2     Running            46 (5d22h ago)     81d
k8s-gateway                 k8s-gateway-779fcd69d9-52jh4                                     1/1     Running            0                  81d
k8s-gateway                 k8s-gateway-779fcd69d9-6v7n7                                     1/1     Running            0                  81d
k8s-gateway                 k8s-gateway-779fcd69d9-qc92j                                     1/1     Running            0                  81d
keycloak                    keycloak-0                                                       1/1     Running            0                  6d16h
keycloak                    keycloak-operator-856475cfc4-pfs8c                               1/1     Running            0                  81d
keycloak                    keycloak-realm-operator-868644b9b8-nsf8k                         1/1     Running            0                  81d
keycloak                    postgres-0                                                       1/1     Running            0                  81d
keycloak                    sylva-5j6th                                                      0/1     Completed          0                  81d
kube-job                    cluster-creator-login-flux-system-wmqpt                          0/1     Completed          0                  81d
kube-job                    keycloak-add-client-scope-job-keycloak-m7cng                     0/1     Completed          0                  81d
kube-job                    pause-cluster-reconciliation-job-default-2zcnt                   0/1     Completed          0                  81d
kube-job                    resume-cluster-reconciliation-job-default-zmq98                  0/1     Completed          0                  81d
kube-system                 etcd-management-cluster-cp-296cbadc7e-5z9mx                      1/1     Running            0                  81d
kube-system                 etcd-management-cluster-cp-296cbadc7e-8sk47                      1/1     Running            0                  81d
kube-system                 etcd-management-cluster-cp-296cbadc7e-zrrm6                      1/1     Running            0                  81d
kube-system                 helm-install-rke2-calico-crd-7zstv                               0/1     Completed          0                  81d
kube-system                 helm-install-rke2-calico-ln9md                                   0/1     Completed          2                  81d
kube-system                 helm-install-rke2-coredns-w9vs6                                  0/1     Completed          0                  81d
kube-system                 helm-install-rke2-ingress-nginx-prg4z                            0/1     Completed          0                  81d
kube-system                 helm-install-rke2-metrics-server-fgzdj                           0/1     Completed          0                  81d
kube-system                 helm-install-rke2-snapshot-controller-crd-5x8qs                  0/1     Completed          0                  81d
kube-system                 helm-install-rke2-snapshot-controller-pfvr6                      0/1     Completed          1 (81d ago)        81d
kube-system                 helm-install-rke2-snapshot-validation-webhook-bbz8g              0/1     Completed          0                  81d
kube-system                 kube-apiserver-management-cluster-cp-296cbadc7e-5z9mx            1/1     Running            1 (6d16h ago)      81d
kube-system                 kube-apiserver-management-cluster-cp-296cbadc7e-8sk47            1/1     Running            1                  81d
kube-system                 kube-apiserver-management-cluster-cp-296cbadc7e-zrrm6            1/1     Running            1 (6d16h ago)      81d
kube-system                 kube-controller-manager-management-cluster-cp-296cbadc7e-5z9mx   1/1     Running            23 (5d22h ago)     81d
kube-system                 kube-controller-manager-management-cluster-cp-296cbadc7e-8sk47   1/1     Running            23 (6d15h ago)     81d
kube-system                 kube-controller-manager-management-cluster-cp-296cbadc7e-zrrm6   1/1     Running            26 (6d16h ago)     81d
kube-system                 kube-proxy-management-cluster-cp-296cbadc7e-5z9mx                1/1     Running            0                  81d
kube-system                 kube-proxy-management-cluster-cp-296cbadc7e-8sk47                1/1     Running            1 (6d16h ago)      6d16h
kube-system                 kube-proxy-management-cluster-cp-296cbadc7e-zrrm6                1/1     Running            0                  81d
kube-system                 kube-proxy-management-cluster-md-md0-592eae0ef3-7kcf6            1/1     Running            0                  81d
kube-system                 kube-scheduler-management-cluster-cp-296cbadc7e-5z9mx            1/1     Running            21 (6d16h ago)     81d
kube-system                 kube-scheduler-management-cluster-cp-296cbadc7e-8sk47            1/1     Running            23 (6d16h ago)     81d
kube-system                 kube-scheduler-management-cluster-cp-296cbadc7e-zrrm6            1/1     Running            28 (5d22h ago)     81d
kube-system                 local-path-provisioner-7c4d8b76f7-frw78                          1/1     Running            0                  81d
kube-system                 rke2-coredns-rke2-coredns-5d8f8494b5-69pnz                       1/1     Running            0                  81d
kube-system                 rke2-coredns-rke2-coredns-5d8f8494b5-xjp7x                       1/1     Running            0                  81d
kube-system                 rke2-coredns-rke2-coredns-autoscaler-65b5bfc754-5qzrq            1/1     Running            0                  81d
kube-system                 rke2-ingress-nginx-controller-5rpfb                              1/1     Running            0                  81d
kube-system                 rke2-ingress-nginx-controller-f825t                              1/1     Running            0                  81d
kube-system                 rke2-ingress-nginx-controller-gmv87                              1/1     Running            0                  81d
kube-system                 rke2-ingress-nginx-controller-xs7vf                              1/1     Running            0                  81d
kube-system                 rke2-metrics-server-5bf59cdccb-vgfpb                             1/1     Running            0                  81d
kube-system                 rke2-snapshot-controller-6f7bbb497d-hpxrr                        1/1     Running            30 (5d22h ago)     81d
kube-system                 rke2-snapshot-validation-webhook-65b5675d5c-tddjv                1/1     Running            0                  81d
kyverno                     kyverno-admission-controller-76d6499cb-cnqrv                     1/1     Running            0                  81d
kyverno                     kyverno-background-controller-67dbb46bd6-j2gph                   1/1     Running            0                  81d
kyverno                     kyverno-cleanup-admission-reports-28463660-zgtdx                 0/1     Completed          0                  6m38s
kyverno                     kyverno-cleanup-cluster-admission-reports-28463660-mpwfz         0/1     Completed          0                  6m38s
kyverno                     kyverno-cleanup-controller-5c9fdbbc5c-g4jdq                      1/1     Running            49 (5d22h ago)     81d
kyverno                     kyverno-reports-controller-7c67c6645d-27mlv                      1/1     Running            49 (5d22h ago)     81d
metal3-system               baremetal-operator-controller-manager-7d48b5ff7c-njp9v           2/2     Running            44 (5d22h ago)     81d
metal3-system               ironic-7f9d4788df-qqzw9                                          3/3     Running            0                  81d
metal3-system               metal3-mariadb-0                                                 1/1     Running            0                  81d
metallb-system              helm-install-metallb-xn87h                                       0/1     Completed          0                  81d
metallb-system              metallb-controller-7866b59f8d-vz76q                              1/1     Running            0                  81d
metallb-system              metallb-speaker-5nnlp                                            1/1     Running            0                  81d
metallb-system              metallb-speaker-bvvg4                                            1/1     Running            0                  81d
metallb-system              metallb-speaker-k2cbn                                            1/1     Running            0                  81d
os-images                   os-image-server-ubuntu-jammy-plain-rke2-1-26-9-0                 1/1     Running            0                  81d
rke2-bootstrap-system       rke2-bootstrap-controller-manager-68c4bc48f7-tmvmb               1/1     Running            52 (5d22h ago)     81d
rke2-control-plane-system   rke2-control-plane-controller-manager-85bc4bc7c9-5zk64           1/1     Running            53 (5d22h ago)     81d
tigera-operator             tigera-operator-6869bc46c4-ldsgf                                 1/1     Running            44 (5d22h ago)     81d
vault                       vault-0                                                          2/2     Running            0                  81d
vault                       vault-1                                                          2/2     Running            0                  81d
vault                       vault-2                                                          2/2     Running            0                  81d
vault                       vault-config-operator-6d88df57cd-752c6                           2/2     Running            38 (5d22h ago)     81d
vault                       vault-configurer-84f654b888-mnldx                                1/1     Running            0                  81d
vault                       vault-operator-6c844f848f-vhj7p                                  1/1     Running            136 (5d22h ago)    81d
```

### Evidence of bare metal workload deployment
```
ubuntu@bootstrap:~/sylva-core$ kubectl get pods -A
NAMESPACE                  NAME                                                              READY   STATUS             RESTARTS           AGE
calico-system              calico-kube-controllers-8bb9845d8-dkthq                           1/1     Running            0                  80d
calico-system              calico-node-54ff9                                                 1/1     Running            0                  60d
calico-system              calico-node-9bcbt                                                 1/1     Running            0                  80d
calico-system              calico-node-pxxbx                                                 1/1     Running            0                  80d
calico-system              calico-node-zmxbm                                                 1/1     Running            0                  40d
calico-system              calico-node-zw2fr                                                 1/1     Running            0                  80d
calico-system              calico-typha-5c7cc9f6fc-6gbqh                                     1/1     Running            0                  80d
calico-system              calico-typha-5c7cc9f6fc-9cjl6                                     1/1     Running            0                  80d
calico-system              calico-typha-5c7cc9f6fc-blsv2                                     1/1     Running            0                  40d
cattle-fleet-system        fleet-agent-855db48487-4zbxq                                      0/1     ImagePullBackOff   0                  6d15h
cattle-monitoring-system   alertmanager-rancher-monitoring-alertmanager-0                    2/2     Running            1 (80d ago)        80d
cattle-monitoring-system   prometheus-rancher-monitoring-prometheus-0                        2/3     CrashLoopBackOff   8084 (82s ago)     80d
cattle-monitoring-system   rancher-monitoring-grafana-768dbccd5d-2dfhg                       4/4     Running            0                  80d
cattle-monitoring-system   rancher-monitoring-kube-state-metrics-56b4477cc-ltdl7             1/1     Running            0                  80d
cattle-monitoring-system   rancher-monitoring-operator-c66c76fd9-zrg7h                       1/1     Running            0                  80d
cattle-monitoring-system   rancher-monitoring-prometheus-adapter-7494f789f6-trh7p            1/1     Running            0                  80d
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-22v7j                 1/1     Running            0                  40d
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-b8q2g                 1/1     Running            0                  60d
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-f8q7c                 1/1     Running            0                  80d
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-gthcz                 1/1     Running            0                  80d
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-v9wpf                 1/1     Running            0                  80d
cattle-system              cattle-cluster-agent-5ff6b5c78f-g2hdl                             1/1     Running            0                  16h
cattle-system              cattle-cluster-agent-5ff6b5c78f-m2szr                             1/1     Running            0                  16h
cattle-system              rancher-webhook-85b57d6bf8-h7tkv                                  1/1     Running            0                  80d
kube-system                etcd-cluster-control-plane-bx92s                                  1/1     Running            0                  80d
kube-system                etcd-cluster-control-plane-mndp5                                  1/1     Running            0                  80d
kube-system                etcd-cluster-control-plane-w6wpr                                  1/1     Running            0                  80d
kube-system                helm-install-rke2-calico-crd-wxqts                                0/1     Completed          0                  80d
kube-system                helm-install-rke2-calico-mpnp2                                    0/1     Completed          0                  80d
kube-system                helm-install-rke2-coredns-ptz7k                                   0/1     Completed          0                  80d
kube-system                helm-install-rke2-ingress-nginx-w62qm                             0/1     Completed          0                  80d
kube-system                helm-install-rke2-metrics-server-9zldx                            0/1     Completed          0                  80d
kube-system                helm-install-rke2-snapshot-controller-crd-rcnp5                   0/1     Completed          0                  80d
kube-system                helm-install-rke2-snapshot-controller-vgmlt                       0/1     Completed          2                  80d
kube-system                helm-install-rke2-snapshot-validation-webhook-d49cp               0/1     Completed          0                  80d
kube-system                kube-apiserver-cluster-control-plane-bx92s                        1/1     Running            0                  80d
kube-system                kube-apiserver-cluster-control-plane-mndp5                        1/1     Running            0                  80d
kube-system                kube-apiserver-cluster-control-plane-w6wpr                        1/1     Running            0                  80d
kube-system                kube-controller-manager-cluster-control-plane-bx92s               1/1     Running            0                  80d
kube-system                kube-controller-manager-cluster-control-plane-mndp5               1/1     Running            0                  80d
kube-system                kube-controller-manager-cluster-control-plane-w6wpr               1/1     Running            0                  80d
kube-system                kube-proxy-cluster-control-plane-bx92s                            1/1     Running            0                  80d
kube-system                kube-proxy-cluster-control-plane-mndp5                            1/1     Running            0                  80d
kube-system                kube-proxy-cluster-control-plane-w6wpr                            1/1     Running            0                  80d
kube-system                kube-proxy-cluster-md0-j6kj8-tgwrt                                1/1     Running            0                  40d
kube-system                kube-proxy-cluster-md0-j6kj8-w6lxs                                1/1     Running            0                  60d
kube-system                kube-scheduler-cluster-control-plane-bx92s                        1/1     Running            0                  80d
kube-system                kube-scheduler-cluster-control-plane-mndp5                        1/1     Running            0                  80d
kube-system                kube-scheduler-cluster-control-plane-w6wpr                        1/1     Running            0                  80d
kube-system                multus-ds-6lcth                                                   1/1     Running            0                  80d
kube-system                multus-ds-j2dz6                                                   1/1     Running            0                  40d
kube-system                multus-ds-svlvz                                                   1/1     Running            0                  60d
kube-system                multus-ds-vr2p2                                                   1/1     Running            0                  80d
kube-system                multus-ds-xzmn8                                                   1/1     Running            0                  80d
kube-system                multus-rke2-whereabouts-fp6xz                                     1/1     Running            0                  60d
kube-system                multus-rke2-whereabouts-fwl6c                                     1/1     Running            0                  40d
kube-system                multus-rke2-whereabouts-q42t5                                     1/1     Running            0                  80d
kube-system                multus-rke2-whereabouts-rb9sv                                     1/1     Running            0                  80d
kube-system                multus-rke2-whereabouts-s9q6m                                     1/1     Running            0                  80d
kube-system                rke2-coredns-rke2-coredns-5d8f8494b5-2rts6                        1/1     Running            0                  40d
kube-system                rke2-coredns-rke2-coredns-5d8f8494b5-9r4qk                        1/1     Running            0                  80d
kube-system                rke2-coredns-rke2-coredns-5d8f8494b5-rxd9n                        1/1     Running            0                  80d
kube-system                rke2-coredns-rke2-coredns-autoscaler-65b5bfc754-xm7b6             1/1     Running            0                  80d
kube-system                rke2-ingress-nginx-controller-8brzw                               1/1     Running            0                  80d
kube-system                rke2-ingress-nginx-controller-fjpgb                               1/1     Running            0                  60d
kube-system                rke2-ingress-nginx-controller-gkrns                               1/1     Running            0                  40d
kube-system                rke2-ingress-nginx-controller-n28rc                               1/1     Running            0                  80d
kube-system                rke2-ingress-nginx-controller-wnvzs                               1/1     Running            0                  80d
kube-system                rke2-metrics-server-5bf59cdccb-n9hzq                              1/1     Running            0                  80d
kube-system                rke2-snapshot-controller-6f7bbb497d-bjf6k                         1/1     Running            0                  80d
kube-system                rke2-snapshot-validation-webhook-65b5675d5c-2zkvx                 1/1     Running            0                  80d
longhorn-system            csi-attacher-7f97fbfccd-8q4hl                                     1/1     Running            0                  80d
longhorn-system            csi-attacher-7f97fbfccd-wr7rc                                     1/1     Running            0                  80d
longhorn-system            csi-attacher-7f97fbfccd-wvwxw                                     1/1     Running            0                  80d
longhorn-system            csi-provisioner-85464b4d95-7v42l                                  1/1     Running            0                  80d
longhorn-system            csi-provisioner-85464b4d95-bh5rp                                  1/1     Running            0                  80d
longhorn-system            csi-provisioner-85464b4d95-l7v4k                                  1/1     Running            0                  80d
longhorn-system            csi-resizer-7f8f66749d-cfbqj                                      1/1     Running            0                  80d
longhorn-system            csi-resizer-7f8f66749d-d7xjm                                      1/1     Running            0                  80d
longhorn-system            csi-resizer-7f8f66749d-h9m69                                      1/1     Running            0                  80d
longhorn-system            csi-snapshotter-6fbd97c949-d9z44                                  1/1     Running            0                  80d
longhorn-system            csi-snapshotter-6fbd97c949-fj27x                                  1/1     Running            0                  80d
longhorn-system            csi-snapshotter-6fbd97c949-qgbn9                                  1/1     Running            0                  80d
longhorn-system            engine-image-ei-af246027-8hlxr                                    1/1     Running            0                  80d
longhorn-system            engine-image-ei-af246027-fkd5p                                    1/1     Running            0                  80d
longhorn-system            engine-image-ei-af246027-jdjtf                                    1/1     Running            0                  60d
longhorn-system            engine-image-ei-af246027-ktgxk                                    1/1     Running            0                  40d
longhorn-system            engine-image-ei-af246027-ndqkn                                    1/1     Running            0                  80d
longhorn-system            instance-manager-e-0afd94931f5ace2e342b750917c49dc8               1/1     Running            0                  80d
longhorn-system            instance-manager-e-0c0c368b7a7f532560479c0e15e51656               1/1     Running            0                  40d
longhorn-system            instance-manager-e-5c66c61105e791578f41321f8aefaa30               1/1     Running            0                  80d
longhorn-system            instance-manager-e-632ed9bb62f32654e41ce69521a6c82d               1/1     Running            0                  60d
longhorn-system            instance-manager-e-e0d51cac98868b3222a0018f4710a7ce               1/1     Running            0                  80d
longhorn-system            instance-manager-r-0afd94931f5ace2e342b750917c49dc8               1/1     Running            0                  80d
longhorn-system            instance-manager-r-0c0c368b7a7f532560479c0e15e51656               1/1     Running            0                  40d
longhorn-system            instance-manager-r-5c66c61105e791578f41321f8aefaa30               1/1     Running            0                  80d
longhorn-system            instance-manager-r-632ed9bb62f32654e41ce69521a6c82d               1/1     Running            0                  60d
longhorn-system            instance-manager-r-e0d51cac98868b3222a0018f4710a7ce               1/1     Running            0                  80d
longhorn-system            longhorn-admission-webhook-649f7c79f4-bdv49                       1/1     Running            0                  80d
longhorn-system            longhorn-admission-webhook-649f7c79f4-sgnzj                       1/1     Running            0                  80d
longhorn-system            longhorn-conversion-webhook-6c64fcc7cf-znbw6                      1/1     Running            0                  80d
longhorn-system            longhorn-conversion-webhook-6c64fcc7cf-zpjfx                      1/1     Running            0                  80d
longhorn-system            longhorn-csi-plugin-5tdlx                                         3/3     Running            0                  80d
longhorn-system            longhorn-csi-plugin-8z2pv                                         3/3     Running            0                  60d
longhorn-system            longhorn-csi-plugin-tmk26                                         3/3     Running            0                  80d
longhorn-system            longhorn-csi-plugin-v8l2s                                         3/3     Running            0                  80d
longhorn-system            longhorn-csi-plugin-xzhwg                                         3/3     Running            0                  40d
longhorn-system            longhorn-driver-deployer-f768b6564-vmj7b                          1/1     Running            0                  80d
longhorn-system            longhorn-manager-26dlp                                            1/1     Running            0                  40d
longhorn-system            longhorn-manager-8vhzh                                            1/1     Running            0                  80d
longhorn-system            longhorn-manager-mq8ls                                            1/1     Running            0                  60d
longhorn-system            longhorn-manager-pg6x9                                            1/1     Running            0                  80d
longhorn-system            longhorn-manager-rdppg                                            1/1     Running            0                  80d
longhorn-system            longhorn-recovery-backend-86bccd5cdd-gtxv4                        1/1     Running            0                  80d
longhorn-system            longhorn-recovery-backend-86bccd5cdd-v9mgn                        1/1     Running            0                  80d
longhorn-system            longhorn-ui-5455597686-27wdd                                      1/1     Running            0                  80d
longhorn-system            longhorn-ui-5455597686-djcn8                                      1/1     Running            0                  80d
metallb-system             helm-install-metallb-5hsj2                                        0/1     Completed          0                  40d
metallb-system             metallb-controller-7866b59f8d-qchn2                               1/1     Running            0                  80d
metallb-system             metallb-speaker-cj7c8                                             1/1     Running            0                  40d
metallb-system             metallb-speaker-wm2jd                                             1/1     Running            0                  40d
metallb-system             metallb-speaker-x5xz6                                             1/1     Running            0                  40d
tigera-operator            tigera-operator-6869bc46c4-kl6ws                                  1/1     Running            0                  80d
```

### Evidence of Ericsson SC deployment
```
ubuntu@bootstrap:~/sylva-core$ kubectl get pods -n sc
NAME                                                              READY   STATUS             RESTARTS           AGE
[...]
eric-sc-nlf-b9796bcc5-2fsvw                                       1/1     Running            0                  39d
eric-sc-nlf-b9796bcc5-pkrgs                                       1/1     Running            0                  39d
[...]
eric-scp-manager-6c798cb848-f9hwl                                 1/1     Running            0                  39d
eric-scp-worker-7d4d58ddc7-pvw4d                                  4/4     Running            0                  39d
eric-scp-worker-7d4d58ddc7-rkcd4                                  4/4     Running            0                  39d
[...]
```

### Evidence of Ericsson SC basic functional testing
Validation center is not made for end-to-end validation, its purpose is to ensure a particular CNF can be instanciated on a Sylva cluster. Hence, usually a basic autonomous set of tests are run. For the present CNF, as we had an Ericsson NRF testing plateform available, we decided to go a bit futher and run a few interoperability checks between the SC and the NRF. Nevertheless there were no performance nor availability tests.

The following table presents the selected tests for this setup with the description, the expected result and the result.

|Test|Description|Expected result|Result|
|-|-|-|-|
|Routing based on 3gpp-Sbi-apiRoot|An AUSF uses the service nudm-ueau through SCP with Sbi-apiRoot equal to UDM1|200 OK with body auth|:heavy_check_mark:|
|Routing based on 3gpp-Sbi-apiRoot and 3gpp-Sbi-Routing-Binding|3gpp-Sbi-Target-Apiroot header is present, but the UDM responds with an error 503, so the  SCP route based on the binding information present in the 3gpp-Sbi-Routing-Binding header|200 OK with body auth to the UDM available in the set|:heavy_check_mark:|
|Routing based on 3gpp-Sbi-discovery-nf-set-id load sharing|3gpp-Sbi-Target-Apiroot header is not present, the  SCP route based on the nfset information present in the 3gpp-Sbi-discovery header|200 OK the traffic is shared between both UDM (same priority)|:heavy_check_mark:|
|Routing based on 3gpp-Sbi-discovery-nf-set-id load sharing|3gpp-Sbi-Target-Apiroot header is not present, the  SCP route based on the nfset information present in the 3gpp-Sbi-discovery header|200 OK the traffic is sent to UDM1 (lowest priority)|:heavy_check_mark:|
|Mediation deleting  a field in outgoing traffic|The mediation rule deletes a field in the body sent by the UDM in the egress response|200 OK with body auth but without the supi field information.|:heavy_check_mark:|
|Mediation adding a header in incoming traffic|The mediation rule adds a new header in the ingress request after sending it to the UDM|The SCP applies the mediation rule by adding a new header "test: Mediation" sent by the AUSF to the UDM|:heavy_check_mark:|
|Mediation adding  a new field in outgoing traffic|The mediation rule adds a new field in the body sent by the UDM in the egress response|200 OK with body auth with a new field "akmaInd"|:heavy_check_mark:|
|Load sharing using delegate discovery|Send traffic to both UDM, same configuration|SCP must share the traffic|:heavy_check_mark:|
|Load sharing using delegate discovery|Load balancing based on priority|SCP should choose the lowest priority|:heavy_check_mark:|
|Load sharing using delegate discovery|Load balancing based on capacity|SCP should share the traffic regarding the capacity of each UDM|:heavy_check_mark:|
|Load sharing using delegate discovery|Load balancing based on load|SCP must share the traffic depending on the weight of each UDM|:heavy_check_mark:|
|Routing based on user identity SUPI|The SCP routes the request based on user identity (SUPI)|200 OK generate-auth-data sent to the UDM after the discovery|:heavy_check_mark:|
|Routing based on delegate discovery nf instance id|SCP will perform delegated discovery based on the discovery headers present in the service request using the instance ID|200 OK generate-auth-data sent to the UDM after the discovery|:heavy_check_mark:|
|Routing based on delegate discovery without requester-nf-type|SCP will perform delegated discovery based on the discovery headers present in the service request without a mandatory header requester nf type|400 Bad Request {"title":"Mandatory field requester-nf-type doesn't exist in SearchRequest","invalidParams":[{"param":"requester-nf-type","reason":"Mandatory field requester-nf-type doesn't exist in SearchRequest"}],"cause":"MANDATORY_QUERY_PARAM_MISSING"}...|:heavy_check_mark:|
|SCP Registration |Verify the SCP register in the NRF|It was verified manually in the SCP configuration and using a discovery request to the NRF|:heavy_check_mark:|
|UDM nf-pool creation|Create the nf-pool udm_dynamic_pool|It was manually verified that the SCP retrieved the UDM profiles based on the nf-pool configuration udm_dynamic_pool|:heavy_check_mark:|

As an exemple, the following is the trace of the test `Routing based on user identity SUPI`
```
curl --http2-prior-knowledge -v -H '3gpp-sbi-discovery-requester-nf-type: AUSF' -H '3gpp-sbi-discovery-supi: imsi-208010000000001' -H '3gpp-sbi-discovery-service-names: nudm-ueau' -H '3gpp-sbi-discovery-target-nf-type: UDM' -H 'Content-Type: application/json' -H 'Accept: application/json' -d '{"ausfInstanceId": "fca61572-5126-11ed-bdc3-0242ac120000","servingNetworkName": "5G_testool:mnc001.mcc208.3gppnetwork.org"}' 'http://192.168.51.10:80/nudm-ueau/v1/imsi-208010000000001/security-information/generate-auth-data'
*   Trying 192.168.51.10:80...
* Connected to 192.168.51.10 (192.168.51.10) port 80
* [HTTP/2] [1] OPENED stream for http://192.168.51.10:80/nudm-ueau/v1/imsi-208010000000001/security-information/generate-auth-data
* [HTTP/2] [1] [:method: POST]
* [HTTP/2] [1] [:scheme: http]
* [HTTP/2] [1] [:authority: 192.168.51.10]
* [HTTP/2] [1] [:path: /nudm-ueau/v1/imsi-208010000000001/security-information/generate-auth-data]
* [HTTP/2] [1] [user-agent: curl/8.5.0]
* [HTTP/2] [1] [3gpp-sbi-discovery-requester-nf-type: AUSF]
* [HTTP/2] [1] [3gpp-sbi-discovery-supi: imsi-208010000000001]
* [HTTP/2] [1] [3gpp-sbi-discovery-service-names: nudm-ueau]
* [HTTP/2] [1] [3gpp-sbi-discovery-target-nf-type: UDM]
* [HTTP/2] [1] [content-type: application/json]
* [HTTP/2] [1] [accept: application/json]
* [HTTP/2] [1] [content-length: 123]
> POST /nudm-ueau/v1/imsi-208010000000001/security-information/generate-auth-data HTTP/2
> Host: 192.168.51.10
> User-Agent: curl/8.5.0
> 3gpp-sbi-discovery-requester-nf-type: AUSF
> 3gpp-sbi-discovery-supi: imsi-208010000000001
> 3gpp-sbi-discovery-service-names: nudm-ueau
> 3gpp-sbi-discovery-target-nf-type: UDM
> Content-Type: application/json
> Accept: application/json
> Content-Length: 123
>
< HTTP/2 200
< server: nginx/1.22.1
< date: Thu, 22 Feb 2024 14:15:48 GMT
< content-type: application/json
< x-powered-by: PHP/8.0.30
< x-envoy-upstream-service-time: 1
< content-length: 317
<
* Connection #0 to host 192.168.51.10 left intact
{"akmaInd":true,"authType":"5G_AKA","authenticationVector":{"autn":"682E37370A3D8000E0CF1BFA1E29ABD6","avType":"5G_HE_AKA","kausf":"5302C9BB4F50E968899145F9BF683FE965A47251E17FC8DC96F1281689C35536","rand":"95AB5C1BE62E8BB2167CF1E716F446CD","xresStar":"8AEB9C83F5C4736FB70DDF3DD99F8CC8"},"supi":"imsi-208010000000001"}
```

With the simulator:
```
3gpp-sbi-discovery-requester-nf-type: AUSF
3gpp-sbi-discovery-service-names: nudm-ueau
3gpp-sbi-discovery-supi: imsi-208010000000001
3gpp-sbi-discovery-target-nf-type: UDM

INFO Data sent:
--------------------------------------------
{
  "ausfInstanceId": "fca61572-5126-11ed-bdc3-0242ac120000",
  "servingNetworkName": "5G_testool:mnc001.mcc208.3gppnetwork.org"
}
./testool_SCP: line 1132: [: too many arguments

INFO: try 1 OK

INFO: Full result:
--------------------------------------------

[  0.001] Connected
[  0.001] send SETTINGS frame <length=12, flags=0x00, stream_id=0>
          (niv=2)
          [SETTINGS_MAX_CONCURRENT_STREAMS(0x03):100]
          [SETTINGS_INITIAL_WINDOW_SIZE(0x04):65535]
[  0.001] send PRIORITY frame <length=5, flags=0x00, stream_id=3>
          (dep_stream_id=0, weight=201, exclusive=0)
[  0.001] send PRIORITY frame <length=5, flags=0x00, stream_id=5>
          (dep_stream_id=0, weight=101, exclusive=0)
[  0.001] send PRIORITY frame <length=5, flags=0x00, stream_id=7>
          (dep_stream_id=0, weight=1, exclusive=0)
[  0.001] send PRIORITY frame <length=5, flags=0x00, stream_id=9>
          (dep_stream_id=7, weight=1, exclusive=0)
[  0.001] send PRIORITY frame <length=5, flags=0x00, stream_id=11>
          (dep_stream_id=3, weight=1, exclusive=0)
[  0.001] send HEADERS frame <length=236, flags=0x24, stream_id=13>
          ; END_HEADERS | PRIORITY
          (padlen=0, dep_stream_id=11, weight=16, exclusive=0)
          ; Open new stream
          :method: POST
          :path: /nudm-ueau/v1/imsi-208010000000001/security-information/generate-auth-data
          :scheme: http
          :authority: 192.168.51.10
          accept: */*
          accept-encoding: gzip, deflate
          user-agent: nghttp2/1.47.0
          content-length: 122
          content-type: application/json
          3gpp-sbi-discovery-requester-nf-type: AUSF
          3gpp-sbi-discovery-service-names: nudm-ueau
          3gpp-sbi-discovery-supi: imsi-208010000000001
          3gpp-sbi-discovery-target-nf-type: UDM
[  0.001] send DATA frame <length=122, flags=0x01, stream_id=13>
          ; END_STREAM
[  0.003] recv SETTINGS frame <length=24, flags=0x00, stream_id=0>
          (niv=4)
          [SETTINGS_HEADER_TABLE_SIZE(0x01):4096]
          [SETTINGS_ENABLE_CONNECT_PROTOCOL(0x08):1]
          [SETTINGS_MAX_CONCURRENT_STREAMS(0x03):2147483647]
          [SETTINGS_INITIAL_WINDOW_SIZE(0x04):268435456]
[  0.003] recv SETTINGS frame <length=0, flags=0x01, stream_id=0>
          ; ACK
          (niv=0)
[  0.003] recv WINDOW_UPDATE frame <length=4, flags=0x00, stream_id=0>
          (window_size_increment=268369921)
[  0.003] send SETTINGS frame <length=0, flags=0x01, stream_id=0>
          ; ACK
          (niv=0)
[  0.025] recv (stream_id=13) :status: 200
[  0.025] recv (stream_id=13) server: nginx/1.22.1
[  0.025] recv (stream_id=13) date: Thu, 22 Feb 2024 14:17:18 GMT
[  0.025] recv (stream_id=13) content-type: application/json
[  0.025] recv (stream_id=13) x-powered-by: PHP/8.0.30
[  0.025] recv (stream_id=13) x-envoy-upstream-service-time: 2
[  0.025] recv (stream_id=13) content-length: 317
[  0.025] recv HEADERS frame <length=100, flags=0x04, stream_id=13>
          ; END_HEADERS
          (padlen=0)
          ; First response header
{"akmaInd":true,"authType":"5G_AKA","authenticationVector":{"autn":"682E37370A3D8000E0CF1BFA1E29ABD6","avType":"5G_HE_AKA","kausf":"5302C9BB4F50E968899145F9BF683FE965A47251E17FC8DC96F1281689C35536","rand":"95AB5C1BE62E8BB2167CF1E716F446CD","xresStar":"8AEB9C83F5C4736FB70DDF3DD99F8CC8"},"supi":"imsi-208010000000001"}[  0.025] recv DATA frame <length=317, flags=0x01, stream_id=13>
          ; END_STREAM
[  0.025] send GOAWAY frame <length=8, flags=0x00, stream_id=0>
          (last_stream_id=0, error_code=NO_ERROR(0x00), opaque_data(0)=[])

```
