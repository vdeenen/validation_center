# Validation Report for Nokia UPF B-23.5.R1

- Date: **16 - October - 2023**

- Validation Responsible:  Vincent Catros

- Validation Team: **Vincent Catros (Orange), Médéric De Verdilhac (Orange)**

- Result: **PASSED**

- Comments: **N/A**

## Validated Workload

- Provider of CNF:  **Nokia**

- Name of CNF: **UPF**

- Version of CNF: **B-23.5.R1**

- Platform capabilities required:
  
  * [x] CPU pinning
  * [x] HugePages
  * [x] Multus CNI
  * [x] SR-IOV
  * [ ] PTP
  * [ ] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI

## Validation Platform

- Sylva version: **v0.2.0**

- Validation environment code: **ORA-1**

- Flavour used for validation:
  
  * [ ] BareMetal + Vanilla K8s
  * [x] BareMetal + RKE2
  * [ ] OpenStack + Vanilla K8s
  * [ ] OpenStack + RKE2
  * [ ] VMware stack + Vanilla K8s
  * [ ] VMware stack + RKE2

- Hardware used for the platform (servers):

  - SERVER 1: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 2: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 3: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 4: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 5: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  
## Validation Evidences


### Evidence of management cluster deployment

```
ubuntu@bootstrap:~/sylva-core-diod$ kubectl get pods -A
NAMESPACE                   NAME                                                             READY   STATUS      RESTARTS       AGE
calico-system               calico-kube-controllers-868d8d5ccb-2fj9g                         1/1     Running     0              2d3h
calico-system               calico-node-6thrb                                                1/1     Running     0              2d3h
calico-system               calico-node-7w9d8                                                1/1     Running     0              2d3h
calico-system               calico-node-pk267                                                1/1     Running     0              2d3h
calico-system               calico-node-xxqc8                                                1/1     Running     0              2d3h
calico-system               calico-typha-5bbc6d4755-7w2qk                                    1/1     Running     0              2d3h
calico-system               calico-typha-5bbc6d4755-npd8v                                    1/1     Running     0              2d3h
capi-rancher-import         capi-rancher-import-6745974b77-gbxx9                             1/1     Running     0              2d2h
capi-system                 capi-controller-manager-59cdc55984-6jpwn                         1/1     Running     0              2d3h
capm3-system                capm3-controller-manager-5d56789fc7-8rclz                        1/1     Running     0              2d3h
capm3-system                ipam-controller-manager-7bddcb565c-6ntk6                         1/1     Running     0              2d3h
capo-system                 capo-controller-manager-67dc49c6b-lsrbp                          1/1     Running     0              2d3h
cattle-fleet-local-system   fleet-agent-7b6499b8d6-6cblk                                     1/1     Running     0              2d2h
cattle-fleet-system         fleet-controller-95f44b688-l7nx4                                 1/1     Running     0              2d2h
cattle-fleet-system         gitjob-774c4b9b57-5ctsd                                          1/1     Running     0              2d2h
cattle-monitoring-system    alertmanager-rancher-monitoring-alertmanager-0                   2/2     Running     0              2d3h
cattle-monitoring-system    prometheus-rancher-monitoring-prometheus-0                       3/3     Running     0              86m
cattle-monitoring-system    rancher-monitoring-grafana-586df56bff-mf4x7                      3/3     Running     0              2d3h
cattle-monitoring-system    rancher-monitoring-kube-state-metrics-77ddfd789b-8hgtk           1/1     Running     0              2d3h
cattle-monitoring-system    rancher-monitoring-operator-ff6cfcc76-mw6rb                      1/1     Running     0              2d3h
cattle-monitoring-system    rancher-monitoring-prometheus-adapter-79d8db9697-bds48           1/1     Running     0              2d3h
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-jcfjk                1/1     Running     0              2d3h
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-mshpf                1/1     Running     0              2d3h
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-pfxrv                1/1     Running     0              2d3h
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-vcqtz                1/1     Running     0              2d3h
cattle-system               rancher-fdc75c68-4jj2z                                           1/1     Running     0              2d2h
cattle-system               rancher-fdc75c68-5b8v5                                           1/1     Running     0              2d2h
cattle-system               rancher-fdc75c68-f8n22                                           1/1     Running     0              2d2h
cattle-system               rancher-webhook-649758c9b7-gp8hz                                 1/1     Running     0              2d2h
cert-manager                cert-manager-74dc7f9649-95v4k                                    1/1     Running     0              2d3h
cert-manager                cert-manager-cainjector-77457fbc49-6hxdz                         1/1     Running     0              2d3h
cert-manager                cert-manager-webhook-744bdd8c85-w28cm                            1/1     Running     0              2d3h
cinder-csi                  openstack-cinder-csi-controllerplugin-6dc8bfd566-f9x5n           6/6     Running     0              2d3h
cinder-csi                  openstack-cinder-csi-nodeplugin-7cfbh                            3/3     Running     0              2d3h
cinder-csi                  openstack-cinder-csi-nodeplugin-bg6tg                            3/3     Running     0              2d3h
cinder-csi                  openstack-cinder-csi-nodeplugin-lv6qb                            3/3     Running     0              2d3h
cinder-csi                  openstack-cinder-csi-nodeplugin-qvtpc                            3/3     Running     0              2d3h
cis-operator-system         cis-operator-6575fd686c-sf92p                                    1/1     Running     0              2d3h
default                     check-rancher-clusters-job-9hj7x                                 0/1     Error       0              26h
default                     check-rancher-clusters-job-pk57z                                 0/1     Error       0              26h
default                     keycloak-add-client-scope-8xzqz                                  0/1     Completed   0              2d2h
default                     pause-cluster-reconciliation-job-8wzxq                           0/1     Completed   0              2d3h
default                     resume-cluster-reconciliation-job-k2mks                          0/1     Completed   0              2d3h
external-secrets            external-secrets-operator-cert-controller-54b6f58d-dqxj7         1/1     Running     0              2d3h
external-secrets            external-secrets-operator-db74c88fd-9bknq                        1/1     Running     0              2d3h
external-secrets            external-secrets-operator-webhook-69495c5b87-9lts8               1/1     Running     0              2d3h
flux-system                 flux-webui-weave-gitops-7d67f5f4fc-5z9mm                         1/1     Running     0              2d2h
flux-system                 helm-controller-66dd588754-9vr7t                                 1/1     Running     0              2d3h
flux-system                 kustomize-controller-7c4bf6bf4b-mhgh6                            1/1     Running     0              2d3h
flux-system                 notification-controller-55848b68ff-q7lcb                         1/1     Running     0              2d3h
flux-system                 source-controller-f44b688dc-smgcb                                1/1     Running     0              2d3h
heat-operator-system        heat-operator-controller-manager-74884f684c-tzh4s                2/2     Running     0              2d3h
k8s-gateway                 k8s-gateway-6685b9d644-sxphh                                     1/1     Running     0              2d3h
k8s-gateway                 k8s-gateway-6685b9d644-tzzhz                                     1/1     Running     0              2d3h
k8s-gateway                 k8s-gateway-6685b9d644-xc2wj                                     1/1     Running     0              2d3h
keycloak                    keycloak-0                                                       1/1     Running     0              2d2h
keycloak                    keycloak-operator-697b4c49bb-gmbjv                               1/1     Running     0              2d2h
keycloak                    keycloak-realm-operator-6bc8f65c7f-j9thg                         1/1     Running     0              2d2h
keycloak                    postgres-0                                                       1/1     Running     0              2d3h
keycloak                    sylva-cm5vp                                                      0/1     Completed   0              2d2h
kube-system                 etcd-management-cluster-cp-101b9cd4e8-dzjxh                      1/1     Running     0              2d3h
kube-system                 etcd-management-cluster-cp-101b9cd4e8-fxtft                      1/1     Running     0              2d3h
kube-system                 etcd-management-cluster-cp-101b9cd4e8-sv478                      1/1     Running     0              2d3h
kube-system                 helm-install-rke2-calico-crd-5lk24                               0/1     Completed   0              2d3h
kube-system                 helm-install-rke2-calico-jx9qx                                   0/1     Completed   2              2d3h
kube-system                 helm-install-rke2-coredns-hpcxz                                  0/1     Completed   0              2d3h
kube-system                 helm-install-rke2-ingress-nginx-qlvwj                            0/1     Completed   0              2d3h
kube-system                 helm-install-rke2-metrics-server-kqqqb                           0/1     Completed   0              2d3h
kube-system                 kube-apiserver-management-cluster-cp-101b9cd4e8-dzjxh            1/1     Running     0              2d3h
kube-system                 kube-apiserver-management-cluster-cp-101b9cd4e8-fxtft            1/1     Running     0              2d3h
kube-system                 kube-apiserver-management-cluster-cp-101b9cd4e8-sv478            1/1     Running     0              2d3h
kube-system                 kube-controller-manager-management-cluster-cp-101b9cd4e8-dzjxh   1/1     Running     0              2d3h
kube-system                 kube-controller-manager-management-cluster-cp-101b9cd4e8-fxtft   1/1     Running     0              2d3h
kube-system                 kube-controller-manager-management-cluster-cp-101b9cd4e8-sv478   1/1     Running     0              2d3h
kube-system                 kube-proxy-management-cluster-cp-101b9cd4e8-dzjxh                1/1     Running     0              2d3h
kube-system                 kube-proxy-management-cluster-cp-101b9cd4e8-fxtft                1/1     Running     0              2d3h
kube-system                 kube-proxy-management-cluster-cp-101b9cd4e8-sv478                1/1     Running     0              2d3h
kube-system                 kube-proxy-management-cluster-md-md0-3844bcfa17-mrwn2            1/1     Running     0              2d3h
kube-system                 kube-scheduler-management-cluster-cp-101b9cd4e8-dzjxh            1/1     Running     0              2d3h
kube-system                 kube-scheduler-management-cluster-cp-101b9cd4e8-fxtft            1/1     Running     0              2d3h
kube-system                 kube-scheduler-management-cluster-cp-101b9cd4e8-sv478            1/1     Running     0              2d3h
kube-system                 local-path-provisioner-798786479d-kq2fx                          1/1     Running     0              2d3h
kube-system                 rke2-coredns-rke2-coredns-58fd75f64b-7nzgm                       1/1     Running     0              2d3h
kube-system                 rke2-coredns-rke2-coredns-58fd75f64b-htlbl                       1/1     Running     0              2d3h
kube-system                 rke2-coredns-rke2-coredns-autoscaler-768bfc5985-b6xw6            1/1     Running     0              2d3h
kube-system                 rke2-ingress-nginx-controller-2jb7r                              1/1     Running     0              2d3h
kube-system                 rke2-ingress-nginx-controller-947pc                              1/1     Running     0              2d3h
kube-system                 rke2-ingress-nginx-controller-vr2bv                              1/1     Running     0              2d3h
kube-system                 rke2-ingress-nginx-controller-wrbt2                              1/1     Running     0              2d3h
kube-system                 rke2-metrics-server-74f878b999-7qplp                             1/1     Running     0              2d3h
kyverno                     kyverno-admission-controller-884f7957c-drttx                     1/1     Running     0              2d3h
kyverno                     kyverno-background-controller-658d5544f8-sqzhw                   1/1     Running     0              2d3h
kyverno                     kyverno-cleanup-admission-reports-28296740-ggw5r                 0/1     Completed   0              6m39s
kyverno                     kyverno-cleanup-cluster-admission-reports-28296740-zs4v7         0/1     Completed   0              6m39s
kyverno                     kyverno-cleanup-controller-5cd69ccc4b-d7kxv                      1/1     Running     0              2d3h
kyverno                     kyverno-reports-controller-754778bbbd-gsq5v                      1/1     Running     0              2d3h
metal3-system               baremetal-operator-controller-manager-5b75967fc6-ntwmk           2/2     Running     0              46h
metal3-system               ironic-5cf6b8549d-hj7cj                                          3/3     Running     0              46h
metal3-system               metal3-mariadb-primary-0                                         1/1     Running     0              46h
metal3-system               metal3-mariadb-secondary-0                                       1/1     Running     0              46h
metallb-system              helm-install-metallb-rcmpl                                       0/1     Completed   0              2d3h
metallb-system              metallb-controller-6bb94c65c5-p2g6z                              1/1     Running     1 (2d3h ago)   2d3h
metallb-system              metallb-speaker-6pjd8                                            1/1     Running     0              2d3h
metallb-system              metallb-speaker-9rzcl                                            1/1     Running     0              2d3h
metallb-system              metallb-speaker-w9fwc                                            1/1     Running     0              2d3h
os-images                   os-image-server-ubuntu-2204-plain-qcow2-0                        1/1     Running     0              2d2h
rke2-bootstrap-system       rke2-bootstrap-controller-manager-6d5bd68958-qk4jf               1/1     Running     0              2d3h
rke2-control-plane-system   rke2-control-plane-controller-manager-757768647d-fkxfr           1/1     Running     0              2d3h
tigera-operator             tigera-operator-7cc7df76d5-p784j                                 1/1     Running     0              2d3h
vault                       vault-0                                                          2/2     Running     0              2d3h
vault                       vault-1                                                          2/2     Running     3 (2d2h ago)   2d2h
vault                       vault-2                                                          2/2     Running     0              2d2h
vault                       vault-config-operator-857dcdbff4-lzvjw                           2/2     Running     0              2d3h
vault                       vault-configurer-7bcc994975-hvr4t                                1/1     Running     0              2d3h
vault                       vault-operator-876f4b9d-fh7ln
```

### Evidence of bare metal workload deployment

```
ubuntu@bootstrap:~/sylva-core-diod$ kubectl get pods -A
NAMESPACE                  NAME                                                                     READY   STATUS      RESTARTS      AGE
calico-system              calico-kube-controllers-868d8d5ccb-cwlbg                                 1/1     Running     0             27h
calico-system              calico-node-7sfk9                                                        1/1     Running     0             27h
calico-system              calico-node-fxcdf                                                        1/1     Running     0             27h
calico-system              calico-node-gjhl4                                                        1/1     Running     0             25h
calico-system              calico-node-lr2k9                                                        1/1     Running     0             26h
calico-system              calico-node-wdpzj                                                        1/1     Running     0             26h
calico-system              calico-typha-64ccc44f7d-5fjxt                                            1/1     Running     0             25h
calico-system              calico-typha-64ccc44f7d-7rr57                                            1/1     Running     0             26h
calico-system              calico-typha-64ccc44f7d-p6qcw                                            1/1     Running     0             27h
cattle-fleet-system        fleet-agent-847ffcbbb-h7x6w                                              1/1     Running     0             25h
cattle-monitoring-system   rancher-monitoring-admission-create-d4jpv                                0/1     Completed   0             26h
cattle-sriov-system        sriov-574d587f59-qsxvt                                                   1/1     Running     0             25h
cattle-sriov-system        sriov-device-plugin-gsdmm                                                1/1     Running     0             25h
cattle-sriov-system        sriov-device-plugin-n28ww                                                1/1     Running     0             25h
cattle-sriov-system        sriov-device-plugin-pxf6n                                                1/1     Running     0             25h
cattle-sriov-system        sriov-device-plugin-qbggl                                                1/1     Running     0             25h
cattle-sriov-system        sriov-device-plugin-tkt4m                                                1/1     Running     0             25h
cattle-sriov-system        sriov-network-config-daemon-52lzv                                        3/3     Running     0             26h
cattle-sriov-system        sriov-network-config-daemon-cvfnr                                        3/3     Running     0             25h
cattle-sriov-system        sriov-network-config-daemon-pdv8v                                        3/3     Running     0             26h
cattle-sriov-system        sriov-network-config-daemon-wjtvn                                        3/3     Running     0             26h
cattle-sriov-system        sriov-network-config-daemon-xc9xc                                        3/3     Running     0             26h
cattle-sriov-system        sriov-rancher-nfd-master-7468d5b9c8-zhxxp                                1/1     Running     0             26h
cattle-sriov-system        sriov-rancher-nfd-worker-cjdtr                                           1/1     Running     0             25h
cattle-sriov-system        sriov-rancher-nfd-worker-kcwk2                                           1/1     Running     0             26h
cattle-sriov-system        sriov-rancher-nfd-worker-ptr42                                           1/1     Running     0             26h
cattle-sriov-system        sriov-rancher-nfd-worker-sk5l8                                           1/1     Running     1 (26h ago)   26h
cattle-sriov-system        sriov-rancher-nfd-worker-tpq68                                           1/1     Running     0             26h
cattle-system              cattle-cluster-agent-6ffd598f94-2gtz9                                    1/1     Running     0             25h
cattle-system              cattle-cluster-agent-6ffd598f94-msj4s                                    1/1     Running     0             102m
cattle-system              rancher-webhook-744c69455f-n9ndw                                         1/1     Running     0             25h
kube-system                etcd-first-workload-cluster-control-plane-g84rj                          1/1     Running     0             27h
kube-system                etcd-first-workload-cluster-control-plane-q5sp7                          1/1     Running     0             27h
kube-system                etcd-first-workload-cluster-control-plane-vwjr6                          1/1     Running     6 (25h ago)   25h
kube-system                helm-install-rke2-calico-crd-wqdzw                                       0/1     Completed   0             27h
kube-system                helm-install-rke2-calico-mczrh                                           0/1     Completed   2             27h
kube-system                helm-install-rke2-coredns-s5znl                                          0/1     Completed   0             27h
kube-system                helm-install-rke2-ingress-nginx-lrb5p                                    0/1     Completed   0             27h
kube-system                helm-install-rke2-metrics-server-jzft6                                   0/1     Completed   0             27h
kube-system                kube-apiserver-first-workload-cluster-control-plane-g84rj                1/1     Running     0             27h
kube-system                kube-apiserver-first-workload-cluster-control-plane-q5sp7                1/1     Running     0             27h
kube-system                kube-apiserver-first-workload-cluster-control-plane-vwjr6                1/1     Running     0             25h
kube-system                kube-controller-manager-first-workload-cluster-control-plane-g84rj       1/1     Running     0             27h
kube-system                kube-controller-manager-first-workload-cluster-control-plane-q5sp7       1/1     Running     0             27h
kube-system                kube-controller-manager-first-workload-cluster-control-plane-vwjr6       1/1     Running     0             25h
kube-system                kube-proxy-first-workload-cluster-control-plane-g84rj                    1/1     Running     0             27h
kube-system                kube-proxy-first-workload-cluster-control-plane-q5sp7                    1/1     Running     0             27h
kube-system                kube-proxy-first-workload-cluster-control-plane-vwjr6                    1/1     Running     0             25h
kube-system                kube-proxy-first-workload-cluster-worker-md-md0-6c998c5d58xf9dhw-ftvvs   1/1     Running     0             26h
kube-system                kube-proxy-first-workload-cluster-worker-md-md0-6c998c5d58xf9dhw-sc6sf   1/1     Running     0             26h
kube-system                kube-scheduler-first-workload-cluster-control-plane-g84rj                1/1     Running     0             27h
kube-system                kube-scheduler-first-workload-cluster-control-plane-q5sp7                1/1     Running     0             27h
kube-system                kube-scheduler-first-workload-cluster-control-plane-vwjr6                1/1     Running     0             25h
kube-system                multus-ds-95pkc                                                          1/1     Running     0             25h
kube-system                multus-ds-9gw6d                                                          1/1     Running     0             26h
kube-system                multus-ds-lbngf                                                          1/1     Running     0             26h
kube-system                multus-ds-x7rpq                                                          1/1     Running     0             26h
kube-system                multus-ds-xqms9                                                          1/1     Running     0             26h
kube-system                multus-rke2-whereabouts-9jlxf                                            1/1     Running     0             26h
kube-system                multus-rke2-whereabouts-fcbtk                                            1/1     Running     0             26h
kube-system                multus-rke2-whereabouts-ggwpv                                            1/1     Running     0             25h
kube-system                multus-rke2-whereabouts-r4rmn                                            1/1     Running     0             26h
kube-system                multus-rke2-whereabouts-zgh4j                                            1/1     Running     0             26h
kube-system                rke2-coredns-rke2-coredns-58fd75f64b-ffhb2                               1/1     Running     0             25h
kube-system                rke2-coredns-rke2-coredns-58fd75f64b-rkdrm                               1/1     Running     0             27h
kube-system                rke2-coredns-rke2-coredns-58fd75f64b-wlhm7                               1/1     Running     0             27h
kube-system                rke2-coredns-rke2-coredns-autoscaler-768bfc5985-rmmvf                    1/1     Running     0             27h
kube-system                rke2-ingress-nginx-controller-6zpwz                                      1/1     Running     0             26h
kube-system                rke2-ingress-nginx-controller-dmgjh                                      1/1     Running     0             27h
kube-system                rke2-ingress-nginx-controller-pmcg2                                      1/1     Running     0             27h
kube-system                rke2-ingress-nginx-controller-tj4q2                                      1/1     Running     0             25h
kube-system                rke2-ingress-nginx-controller-wp2z8                                      1/1     Running     0             26
kube-system                rke2-metrics-server-74f878b999-4lv7q                                     1/1     Running     0             27h
longhorn-system            csi-attacher-59db6bb96c-859dm                                            1/1     Running     0             25h
longhorn-system            csi-attacher-59db6bb96c-b6kxl                                            1/1     Running     0             25h
longhorn-system            csi-attacher-59db6bb96c-rdltt                                            1/1     Running     0             26h
longhorn-system            csi-provisioner-6d44d79b88-24gsw                                         1/1     Running     0             25h
longhorn-system            csi-provisioner-6d44d79b88-6brgf                                         1/1     Running     0             26h
longhorn-system            csi-provisioner-6d44d79b88-mpzdd                                         1/1     Running     0             25h
longhorn-system            csi-resizer-74cc7c9c95-6zr4r                                             1/1     Running     0             26h
longhorn-system            csi-resizer-74cc7c9c95-8b6nn                                             1/1     Running     0             26h
longhorn-system            csi-resizer-74cc7c9c95-fh49l                                             1/1     Running     0             25h
longhorn-system            csi-snapshotter-7f7685dfdb-9xvjm                                         1/1     Running     0             26h
longhorn-system            csi-snapshotter-7f7685dfdb-z2bt9                                         1/1     Running     0             26h
longhorn-system            csi-snapshotter-7f7685dfdb-z4hf2                                         1/1     Running     0             25h
longhorn-system            engine-image-ei-ef01bf86-2ssv2                                           1/1     Running     0             26h
longhorn-system            engine-image-ei-ef01bf86-f2kpm                                           1/1     Running     0             25h
longhorn-system            engine-image-ei-ef01bf86-h47pj                                           1/1     Running     0             26h
longhorn-system            engine-image-ei-ef01bf86-hhxtk                                           1/1     Running     0             26h
longhorn-system            engine-image-ei-ef01bf86-tk5mj                                           1/1     Running     0             26h
longhorn-system            instance-manager-e-1830d60ec16f2df5542e4c3372f87194                      1/1     Running     0             25h
longhorn-system            instance-manager-e-4f7a1254c3d07a1b6861cd9a0d7e2dfd                      1/1     Running     0             26h
longhorn-system            instance-manager-e-d963fd1377104dced6e6a71b0665bc7f                      1/1     Running     0             26h
longhorn-system            instance-manager-e-e1da9034a5fec936480771f131e3188d                      1/1     Running     0             25h
longhorn-system            instance-manager-e-f47beab463ecba66006e7ba095ca5b20                      1/1     Running     0             25h
longhorn-system            instance-manager-r-1830d60ec16f2df5542e4c3372f87194                      1/1     Running     0             25h
longhorn-system            instance-manager-r-4f7a1254c3d07a1b6861cd9a0d7e2dfd                      1/1     Running     0             26h
longhorn-system            instance-manager-r-d963fd1377104dced6e6a71b0665bc7f                      1/1     Running     0             26h
longhorn-system            instance-manager-r-e1da9034a5fec936480771f131e3188d                      1/1     Running     0             25h
longhorn-system            instance-manager-r-f47beab463ecba66006e7ba095ca5b20                      1/1     Running     0             25h
longhorn-system            longhorn-admission-webhook-5bf468b585-5xtgw                              1/1     Running     0             25h
longhorn-system            longhorn-admission-webhook-5bf468b585-v8dzn                              1/1     Running     0             25h
longhorn-system            longhorn-conversion-webhook-588cfff8b-rj4nq                              1/1     Running     0             25h
longhorn-system            longhorn-conversion-webhook-588cfff8b-wmhhr                              1/1     Running     0             26h
longhorn-system            longhorn-csi-plugin-4cbtp                                                3/3     Running     0             26h
longhorn-system            longhorn-csi-plugin-7gl9d                                                3/3     Running     0             26h
longhorn-system            longhorn-csi-plugin-8mkmp                                                3/3     Running     0             26h
longhorn-system            longhorn-csi-plugin-jwfpt                                                3/3     Running     0             26h
longhorn-system            longhorn-csi-plugin-tk274                                                3/3     Running     0             25h
longhorn-system            longhorn-driver-deployer-6f6dd986dc-jj9ln                                1/1     Running     0             25h
longhorn-system            longhorn-manager-6rqtv                                                   1/1     Running     0             26h
longhorn-system            longhorn-manager-8hr7v                                                   1/1     Running     0             26h
longhorn-system            longhorn-manager-g6nlp                                                   1/1     Running     0             26h
longhorn-system            longhorn-manager-kk72g                                                   1/1     Running     0             26h
longhorn-system            longhorn-manager-w9qbz                                                   1/1     Running     0             25h
longhorn-system            longhorn-recovery-backend-66f74fdf9-cw7w6                                1/1     Running     0             25h
longhorn-system            longhorn-recovery-backend-66f74fdf9-t5rf8                                1/1     Running     0             26h
longhorn-system            longhorn-ui-7d959687bf-kmlhv                                             1/1     Running     0             25h
longhorn-system            longhorn-ui-7d959687bf-xxkms                                             1/1     Running     0             26h
metallb-system             helm-install-metallb-jtnvj                                               0/1     Completed   0             27h
metallb-system             metallb-controller-6bb94c65c5-tpwz8                                      1/1     Running     1 (27h ago)   27h
metallb-system             metallb-speaker-gkhg5                                                    1/1     Running     0             25h
metallb-system             metallb-speaker-ks959                                                    1/1     Running     0             27h
metallb-system             metallb-speaker-sp4m2                                                    1/1     Running     0             27h
tigera-operator            tigera-operator-7cc7df76d5-pmdxn                                         1/1     Running     0             27h
```

### Nokia UPF deployment
The Nokia UPF deployment has been done in 2 steps.
- step 1, a simple deployment without redis database.
- step 2, a deployment with redis database

### Evidence of Nokia UPF deployment (step 1)
We can check Kubernetes resources have been properly deployed.
```
ubuntu@bootstrap:~/upf$ kubectl get all -n upf
NAME                             READY   STATUS    RESTARTS   AGE
pod/llb-statefulset-0            1/1     Running   0          70m
pod/llb-statefulset-1            1/1     Running   0          69m
pod/lmg-statefulset-0            1/1     Running   0          70m
pod/lmg-statefulset-1            1/1     Running   0          69m
pod/loam-a-v1-68ff55bbf8-54pq5   1/1     Running   0          70m
pod/loam-b-v1-867f7cb458-x49sc   1/1     Running   0          70m

NAME                          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                                       AGE
service/llb                   ClusterIP   None            <none>        <none>                                        70m
service/llb-1-connectivity    NodePort    10.43.36.112    <none>        2017:32017/TCP                                70m
service/llb-2-connectivity    NodePort    10.43.10.95     <none>        2018:32018/TCP                                70m
service/lmg                   ClusterIP   None            <none>        <none>                                        70m
service/lmg-1-connectivity    NodePort    10.43.51.17     <none>        2001:32001/TCP                                70m
service/lmg-2-connectivity    NodePort    10.43.46.138    <none>        2002:32002/TCP                                70m
service/loam-a                ClusterIP   None            <none>        <none>                                        70m
service/loam-a-connectivity   NodePort    10.43.168.227   <none>        2021:32021/TCP                                70m
service/loam-b                ClusterIP   None            <none>        <none>                                        70m
service/loam-b-connectivity   NodePort    10.43.127.221   <none>        2022:32022/TCP                                70m
service/loam-connectivity     NodePort    10.43.148.80    <none>        2323:32023/TCP,2222:32221/TCP,164:31164/UDP   70m

NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/loam-a-v1   1/1     1            1           70m
deployment.apps/loam-b-v1   1/1     1            1           70m

NAME                                   DESIRED   CURRENT   READY   AGE
replicaset.apps/loam-a-v1-68ff55bbf8   1         1         1       70m
replicaset.apps/loam-b-v1-867f7cb458   1         1         1       70m

NAME                               READY   AGE
statefulset.apps/llb-statefulset   2/2     70m
statefulset.apps/lmg-statefulset   2/2     70m

NAME                                          REFERENCE                     TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/llb-hpa   StatefulSet/llb-statefulset   26%/90%   2         2         2          70m
horizontalpodautoscaler.autoscaling/lmg-hpa   StatefulSet/lmg-statefulset   18%/90%   2         2         2          70m
```

We can check LLB and LMG are actually using SR-IOV/DPDK
```
ubuntu@bootstrap:~/upf$ kubectl logs -n upf llb-statefulset-0
Opening provided symbol file

    _     _     _____ 
   | |   | |   |  _  \
   | |   | |   | |_| |
   | |___| |___| |_| |
   |_____|_____|_____/

Copyright (c) 2000-2020 Nokia. All rights reserved.

Version:      I-23.5.R1 
Build:        Built on Wed Jun 7 04:08:20 EDT 2023 by gitlab-runner in /home/gitlab-runner/usr/local/gitlab-runner-workdir/builds/se9y-iAQ/0/mg-src/panos/main/linux 
Target Info:  TGT_PF=1 TGT_SYS=7 TGT_HW=7 TGT_CPU=5 TGT_BSP=1 BETA=1 PTR_CHCK=0
CPU Info:     Intel(R) Xeon(R) Gold 6238R CPU @ 2.20GHz
Total Memory: 385544MB
Kernel Info:  5.15.0-76-generic (buildd@lcy02-amd64-028) (gcc (Ubuntu 11.3.0-1ubuntu1~22.04.1) 11.3.0, GNU ld (GNU Binutils for Ubuntu) 2.38) #83-Ubuntu SMP Thu Jun 15 19:16:32 UTC 2023
Process Info: PID=1 MySlotNum=17 ConfigFile=/etc/sysconfig/llb.cfg
RT Sched:     DISABLED
Crash Reboot: DISABLED
Running under Kubernetes with dual-stack DISABLED

Available cores : 5 7 61 63 
Re-mapped cores : 5 7 61 63  Num Cores : 4 Available coremask : 0xa0000000000000a0

sysVmBootStringGenerateInner: bios product string='chassis=VSR card=iom-v mda/1=m20-v ht=3 fswo=300 slot=17'
printf-Will use separate stack for signal handler[019 s 10/20/23 13:39:21.839] 17:msmMainTask:SIM:simPortsMapMdaPorts mapped 17/1/1 -> port p4
[020 s 10/20/23 13:39:22.858] 17:msmMainTask:SIM:simPortsMapMdaPorts mapped 17/1/2 -> port p5
DPDK setting lcore to 63 - now 63
```
```
ubuntu@bootstrap:~/upf$ kubectl logs -n upf lmg-statefulset-0
net.ipv4.conf.default.rp_filter = 0
Opening provided symbol file

    _     __  __  ____
   | |   |  \/  |/ ___|
   | |   | |\/| | |  _
   | |___| |  | | |_| |
   |_____|_|  |_|\____|

Copyright (c) 2000-2020 Nokia. All rights reserved.

Version:      I-23.5.R1 
Build:        Built on Wed Jun 7 04:08:20 EDT 2023 by gitlab-runner in /home/gitlab-runner/usr/local/gitlab-runner-workdir/builds/se9y-iAQ/0/mg-src/panos/main/linux 
Target Info:  TGT_PF=1 TGT_SYS=7 TGT_HW=7 TGT_CPU=5 TGT_BSP=1 BETA=1 PTR_CHCK=0
CPU Info:     Intel(R) Xeon(R) Gold 6238R CPU @ 2.20GHz
Total Memory: 385544MB
Kernel Info:  5.15.0-76-generic (buildd@lcy02-amd64-028) (gcc (Ubuntu 11.3.0-1ubuntu1~22.04.1) 11.3.0, GNU ld (GNU Binutils for Ubuntu) 2.38) #83-Ubuntu SMP Thu Jun 15 19:16:32 UTC 2023
Process Info: PID=8 MySlotNum=1 ConfigFile=/etc/sysconfig/lmg.cfg
RT Sched:     DISABLED
Crash Reboot: DISABLED
Running under Kubernetes with dual-stack DISABLED

Re-mapping coreId-65 to coreId-0
Available cores : 5 7 9 61 63 65 
Re-mapped cores : 0 5 7 9 61 63  Num Cores : 6 Available coremask : 0xa0000000000002a1

sysVmBootStringGenerateInner: bios product string='chassis=VSR card=iom-v-mg mda/1=isa-mg-v mda/2=isa-ms-v mda/3=m20-v ht=3 fswo=300 slot=1'
printf-Will use separate stack for signal handler[033 s 10/20/23 13:39:21.658]  1:msmMainTask:SIM:simPortsMapMdaPorts mapped 1/3/1 -> port p4
[034 s 10/20/23 13:39:22.700]  1:msmMainTask:SIM:simPortsMapMdaPorts mapped 1/3/2 -> port p5
DPDK setting lcore to 63 - now 63
```
 
### Evidence of Nokia UPF basic functional testing 
In order to validate UPF on Sylva, Nokia proposed to make a simple configuration and to ping the DC GW. This should validate SR-IOV/DPDK.

After configuration has been pushed to UPF, we are able to ping the gateway through SR-IOV/DPDK interface
```
A:SylvaUPF# ping 192.168.254.201 router 150 
PING 192.168.254.201 56 data bytes
64 bytes from 192.168.254.201: icmp_seq=1 ttl=64 time=7.94ms.
64 bytes from 192.168.254.201: icmp_seq=2 ttl=64 time=1.18ms.
64 bytes from 192.168.254.201: icmp_seq=3 ttl=64 time=1.04ms.
^C
ping aborted by user

---- 192.168.254.201 PING Statistics ----
3 packets transmitted, 3 packets received, 0.00% packet loss
round-trip min = 1.04ms, avg = 3.39ms, max = 7.94ms, stddev = 0.000ms
```

### Evidence of Nokia UPF deployment (step 2)
We can check CDB Helm chart has been deployed
```
ubuntu@bootstrap:~/upf$ kubectl get all -n cdb
NAME                              READY   STATUS    RESTARTS   AGE
pod/dbproxy-v1-758c99658c-hqsph   1/1     Running   0          34s
pod/redis-statefulset-0           1/1     Running   0          34s
pod/redis-statefulset-1           1/1     Running   0          20s

NAME                           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/dbissu                 ClusterIP   None            <none>        <none>     34s
service/dbproxy                ClusterIP   None            <none>        <none>     34s
service/redis                  ClusterIP   None            <none>        <none>     34s
service/redis-connectivity-0   ClusterIP   10.43.122.228   <none>        6379/TCP   34s
service/redis-connectivity-1   ClusterIP   10.43.213.126   <none>        6379/TCP   34s

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/dbproxy-v1   1/1     1            1           34s

NAME                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/dbproxy-v1-758c99658c   1         1         1       34s

NAME                                 READY   AGE
statefulset.apps/redis-statefulset   2/2     34s

NAME                                            REFERENCE                       TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/redis-hpa   StatefulSet/redis-statefulset   <unknown>/90%   2         2         2          34s
```
