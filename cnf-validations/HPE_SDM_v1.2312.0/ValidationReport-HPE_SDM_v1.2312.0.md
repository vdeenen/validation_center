# Validation Report for HPE SDM v1.2312.0

- Date: **14 - February - 2024**

- Validation Responsible:  **Guillermo Carreto (Telefónica)**

- Validation Team: **Guillermo Carreto (Telefónica) and Laurent Fouilhac (HPE)**

- Result: **PASSED**

- Comments:

## Validated Workload

- Provider of CNF: **HPE**

- Name of CNF: **Subscriber Data Management (SDM) which covers AUSF, UDM, UDR, UDSF and NRF network functions**

- Version of CNF: **v1.2312.0**

- Platform capabilities required:
  
  * [ ] CPU pinning
  * [ ] HugePages
  * [ ] Multus CNI
  * [ ] SR-IOV
  * [ ] PTP
  * [x] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI

## Validation Platform

- Sylva version: **v0.3.1**

- Validation environment used: **TEF-1**

- Flavour used for validation:
  
  * [ ] BareMetal + Vanilla K8s
  * [ ] BareMetal + RKE2
  * [ ] OpenStack + Vanilla K8s
  * [x] OpenStack + RKE2
  * [ ] VMware stack + Vanilla K8s
  * [ ] VMware stack + RKE2

- HW used for the platform (servers):

  - **4 x HPE ProLiant DL380 Gen10 (72 vCPUs, 384GB RAM, 878GB Local Storage)**
  - **2 x HPE ProLiant DL380 Gen10 (72 vCPUs, 576GB RAM, 731GB Local Storage)**
  - **2 x Intel R2208WFTZSR (96 vCPUs, 192GB RAM, 438GB Local Storage)**

- Virtual Machines used for the platform (only applies if a Sylva's IaaS flavour like OpenStack or VMware has been used):
  
  - **CAPI Management Cluster --> 3 x VM: 4 vCPUs, 8GB RAM, 80GB Local Storage**
  - **CAPI Workload Cluster --> 4 x VM: 8 vCPUs, 16GB RAM, 300GB Local Storage**

## Validation Evidences

### Evidence of Platform Configuration

The following is a screenshot of the evidence that the platform was configured with the capabilities required by the workload under validation (in this case, only Calico CNI is required):

```bash
2024-02-15 11:07:31,406 - xtesting.ci.run_tests - INFO - Xtesting report:

+-------------------------------------------+------------------------------+--------------------------------------------------------+------------------+----------------+
|                 TEST CASE                 |           PROJECT            |                          TIER                          |     DURATION     |     RESULT     |
+-------------------------------------------+------------------------------+--------------------------------------------------------+------------------+----------------+
|          Multus CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|          Multus NAD CRD Creation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:01       |      FAIL      |
|          SR-IOV CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|     SR-IOV Device Plugin Installation     |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|        WhereAbouts CNI Installation       |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|          Calico CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      PASS      |
|          Flannel CNI Installation         |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|          Cilium CNI Installation          |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
|         WeaveNet CNI Installation         |     Platform Smoke Tests     |     Sylva Validation Center - Platform Smoke Tests     |      00:00       |      FAIL      |
+-------------------------------------------+------------------------------+--------------------------------------------------------+------------------+----------------+
```

### Evidence of Workload Deployment

Next evidence includes all the objects created due to HPE SDM deployment:

```bash
ubuntu@hpe-wc-cp-e3c8f5319e-x9xvj:~$ kubectl get all -n hpe-5g
NAME                                            READY   STATUS    RESTARTS   AGE
pod/hpe-ausf-nausf-auth-6874b6cb9d-4c7bg        1/1     Running   0          2d16h
pod/hpe-ignite-udr-0                            1/1     Running   0          2d16h
pod/hpe-ignite-udsf-0                           1/1     Running   0          2d16h
pod/hpe-nf-operator-8594d48687-6f5nw            1/1     Running   0          2d16h
pod/hpe-nf-operator-deployment-947744b5-bwhmm   1/1     Running   0          2d16h
pod/hpe-nrf-server-656c6ddd54-hscqh             1/1     Running   0          2d16h
pod/hpe-sde-ignite-operator-7b6757f77b-h9j8g    1/1     Running   0          2d16h
pod/hpe-udm-cmod-encrypt-5c4695c48c-j88jm       1/1     Running   0          2d16h
pod/hpe-udm-cmod-keygen-6f867d44fd-qp2z6        1/1     Running   0          2d16h
pod/hpe-udm-cmod-service-6dd46bd585-7vphz       1/1     Running   0          2d16h
pod/hpe-udm-nudm-ee-77cbfdbc88-k6l7m            1/1     Running   0          2d16h
pod/hpe-udm-nudm-notify-f746dffdc-ffjtl         1/1     Running   0          2d16h
pod/hpe-udm-nudm-sdm-5759bf58db-wj22b           1/1     Running   0          2d16h
pod/hpe-udm-nudm-ueau-f87db75d6-nfnk9           1/1     Running   0          2d16h
pod/hpe-udm-nudm-uecm-6554d5659c-5ssh8          1/1     Running   0          2d16h
pod/hpe-udr-nudr-dr-b4cf54f57-gm5w7             1/1     Running   0          2d16h
pod/hpe-udr-nudr-notify-857cc49bb4-gkfpj        1/1     Running   0          2d16h
pod/hpe-udr-nudr-prov-6bc8d6cfcc-74n6c          1/1     Running   0          2d16h
pod/hpe-udsf-nudsf-dr-5f5b5c8cbc-q4mjv          1/1     Running   0          2d16h
pod/hpe-udsf-nudsf-notify-547d978bf7-tm8c4      1/1     Running   0          2d16h
pod/hpe-udsf-nudsf-oam-794b54d4dd-qkqpr         1/1     Running   0          2d16h

NAME                                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                                                                AGE
service/hpe-ausf-nausf-auth          NodePort    10.43.158.108   <none>        8082:30862/TCP,8080:30100/TCP,9273:31311/TCP                           3d16h
service/hpe-ignite-udr               ClusterIP   None            <none>        11211/TCP,47100/TCP,47500/TCP,49112/TCP,10800/TCP,8080/TCP,10900/TCP   3d16h
service/hpe-ignite-udsf              ClusterIP   None            <none>        11211/TCP,47100/TCP,47500/TCP,49112/TCP,10800/TCP,8080/TCP,10900/TCP   3d16h
service/hpe-nf-operator              ClusterIP   10.43.147.204   <none>        8060/TCP,9273/TCP                                                      3d16h
service/hpe-nf-operator-deployment   ClusterIP   10.43.147.41    <none>        8060/TCP,9273/TCP                                                      3d16h
service/hpe-nrf-server               ClusterIP   10.43.10.125    <none>        8082/TCP,8080/TCP,9273/TCP                                             3d16h
service/hpe-udm-cmod-encrypt         NodePort    10.43.46.70     <none>        8082:31443/TCP,8080:30890/TCP,9273:30413/TCP                           3d16h
service/hpe-udm-cmod-keygen          NodePort    10.43.62.246    <none>        8082:31343/TCP,8080:30892/TCP,9273:31142/TCP                           3d16h
service/hpe-udm-cmod-service         NodePort    10.43.55.87     <none>        8082:32625/TCP,8080:30894/TCP,9273:30994/TCP                           3d16h
service/hpe-udm-nudm-ee              NodePort    10.43.136.66    <none>        8082:32267/TCP,8080:30863/TCP,9273:30903/TCP                           3d16h
service/hpe-udm-nudm-notify          NodePort    10.43.114.67    <none>        8082:31919/TCP,8080:30866/TCP,9273:31604/TCP                           3d16h
service/hpe-udm-nudm-sdm             NodePort    10.43.63.117    <none>        8082:32298/TCP,8080:30867/TCP,9273:32029/TCP                           3d16h
service/hpe-udm-nudm-ueau            NodePort    10.43.252.19    <none>        8082:30096/TCP,8080:30868/TCP,9273:32000/TCP                           3d16h
service/hpe-udm-nudm-uecm            NodePort    10.43.163.133   <none>        8082:30949/TCP,8080:30869/TCP,9273:31023/TCP                           3d16h
service/hpe-udr-nudr-collector       ClusterIP   10.43.166.107   <none>        8082/TCP,8080/TCP,9273/TCP                                             3d16h
service/hpe-udr-nudr-dr              NodePort    10.43.184.253   <none>        8082:32002/TCP,8080:30870/TCP,9273:31171/TCP                           3d16h
service/hpe-udr-nudr-notify          NodePort    10.43.122.80    <none>        8082:31328/TCP,8080:30872/TCP,9273:31579/TCP                           3d16h
service/hpe-udr-nudr-oam             ClusterIP   10.43.239.174   <none>        8082/TCP,8080/TCP,9273/TCP                                             3d16h
service/hpe-udr-nudr-prov            NodePort    10.43.41.126    <none>        8082:30459/TCP,8080:30871/TCP,9273:30786/TCP                           3d16h
service/hpe-udsf-nudsf-dr            NodePort    10.43.249.114   <none>        8082:31243/TCP,8080:30880/TCP,9273:31012/TCP                           3d16h
service/hpe-udsf-nudsf-notify        NodePort    10.43.204.84    <none>        8082:30380/TCP,8080:30881/TCP,9273:30471/TCP                           3d16h
service/hpe-udsf-nudsf-oam           ClusterIP   10.43.207.150   <none>        8082/TCP,8080/TCP,9273/TCP                                             3d16h

NAME                                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hpe-ausf-nausf-auth          1/1     1            1           3d16h
deployment.apps/hpe-nf-operator              1/1     1            1           3d16h
deployment.apps/hpe-nf-operator-deployment   1/1     1            1           3d16h
deployment.apps/hpe-nrf-server               1/1     1            1           3d16h
deployment.apps/hpe-sde-ignite-operator      1/1     1            1           3d16h
deployment.apps/hpe-udm-cmod-encrypt         1/1     1            1           3d16h
deployment.apps/hpe-udm-cmod-keygen          1/1     1            1           3d16h
deployment.apps/hpe-udm-cmod-service         1/1     1            1           3d16h
deployment.apps/hpe-udm-nudm-ee              1/1     1            1           3d16h
deployment.apps/hpe-udm-nudm-notify          1/1     1            1           3d16h
deployment.apps/hpe-udm-nudm-sdm             1/1     1            1           3d16h
deployment.apps/hpe-udm-nudm-ueau            1/1     1            1           3d16h
deployment.apps/hpe-udm-nudm-uecm            1/1     1            1           3d16h
deployment.apps/hpe-udr-nudr-collector       0/1     0            0           3d16h
deployment.apps/hpe-udr-nudr-dr              1/1     1            1           3d16h
deployment.apps/hpe-udr-nudr-notify          1/1     1            1           3d16h
deployment.apps/hpe-udr-nudr-oam             0/1     0            0           3d16h
deployment.apps/hpe-udr-nudr-prov            1/1     1            1           3d16h
deployment.apps/hpe-udsf-nudsf-dr            1/1     1            1           3d16h
deployment.apps/hpe-udsf-nudsf-notify        1/1     1            1           3d16h
deployment.apps/hpe-udsf-nudsf-oam           1/1     1            1           3d16h

NAME                                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/hpe-ausf-nausf-auth-6874b6cb9d          1         1         1       3d16h
replicaset.apps/hpe-nf-operator-8594d48687              1         1         1       2d16h
replicaset.apps/hpe-nf-operator-bf8c4db4c               0         0         0       3d16h
replicaset.apps/hpe-nf-operator-deployment-5df54f88b5   0         0         0       3d16h
replicaset.apps/hpe-nf-operator-deployment-947744b5     1         1         1       2d16h
replicaset.apps/hpe-nrf-server-5f5644cc8c               0         0         0       3d16h
replicaset.apps/hpe-nrf-server-656c6ddd54               1         1         1       2d21h
replicaset.apps/hpe-nrf-server-659857d9cd               0         0         0       2d21h
replicaset.apps/hpe-sde-ignite-operator-75cd97765b      0         0         0       3d13h
replicaset.apps/hpe-sde-ignite-operator-75f88644cc      0         0         0       3d16h
replicaset.apps/hpe-sde-ignite-operator-7b6757f77b      1         1         1       2d16h
replicaset.apps/hpe-udm-cmod-encrypt-5c4695c48c         1         1         1       3d16h
replicaset.apps/hpe-udm-cmod-keygen-6f867d44fd          1         1         1       3d16h
replicaset.apps/hpe-udm-cmod-service-6dd46bd585         1         1         1       3d16h
replicaset.apps/hpe-udm-nudm-ee-77cbfdbc88              1         1         1       3d16h
replicaset.apps/hpe-udm-nudm-notify-f746dffdc           1         1         1       3d16h
replicaset.apps/hpe-udm-nudm-sdm-5759bf58db             1         1         1       3d16h
replicaset.apps/hpe-udm-nudm-ueau-f87db75d6             1         1         1       3d16h
replicaset.apps/hpe-udm-nudm-uecm-6554d5659c            1         1         1       3d16h
replicaset.apps/hpe-udr-nudr-collector-9b7555d4c        1         0         0       3d16h
replicaset.apps/hpe-udr-nudr-dr-b4cf54f57               1         1         1       3d16h
replicaset.apps/hpe-udr-nudr-notify-857cc49bb4          1         1         1       3d16h
replicaset.apps/hpe-udr-nudr-oam-844bbd88f5             1         0         0       3d16h
replicaset.apps/hpe-udr-nudr-prov-6bc8d6cfcc            1         1         1       3d16h
replicaset.apps/hpe-udsf-nudsf-dr-5f5b5c8cbc            1         1         1       3d16h
replicaset.apps/hpe-udsf-nudsf-notify-547d978bf7        1         1         1       3d16h
replicaset.apps/hpe-udsf-nudsf-oam-794b54d4dd           1         1         1       3d16h

NAME                               READY   AGE
statefulset.apps/hpe-ignite-udr    1/1     3d16h
statefulset.apps/hpe-ignite-udsf   1/1     3d16h
```

### Evidence of Workload Basic Functional Testing

The following test cases have been executed:

1. UE Provisioning
2. Authentication SUPI-based, AKA
3. AMF Registration
4. AMF subscription
5. Get AM data from UDM
6. SMF selection
7. SMF registration
8. Get SM data from UDM
9. Get subscriber data from UDM
10. Delete SMF subscription

Every test case has been executed successfully and tests results are available at [HPE SDM Functional Test Report](./HPE%205G%20SDM%20on%20Sylva%20(TEF-1)%20-%20Functional%20Test%20Report%20-%20v0.1.pdf)
