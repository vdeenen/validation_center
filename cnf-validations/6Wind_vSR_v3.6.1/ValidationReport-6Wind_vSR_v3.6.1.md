# Validation Report for 6Wind vSR v3.6.1

- Date: **15 - August - 2023**

- Validation Responsible:  **Vincent Catros (Orange)**

- Validation Team: **Vincent Catros (Orange), Médéric De Verdilhac (Orange), François Regis Menguy (Orange)**

- Result: **PASSED**

- Comments: **This validation raised a few difficulties that could be of interest for Sylva core team. This feedback is exposed in the last section of this report.**

## Validated Workload

- Provider of CNF:  **6Wind**

- Name of CNF: **vSR**

- Version of CNF: **v3.6.1**

- Platform capabilities required:
  
  * [ ] CPU pinning
  * [x] HugePages
  * [x] Multus CNI
  * [x] SR-IOV
  * [ ] PTP
  * [ ] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI

## Validation Platform

- Sylva version: **v0.2.0**

- Validation environment code: **ORA-1**

- Flavour used for validation:
  
  * [ ] BareMetal + Vanilla K8s
  * [x] BareMetal + RKE2
  * [ ] OpenStack + Vanilla K8s
  * [ ] OpenStack + RKE2
  * [ ] VMware stack + Vanilla K8s
  * [ ] VMware stack + RKE2

- Hardware used for the platform (servers):

  - SERVER 1: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 2: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 3: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 4: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS
  - SERVER 5: Dell, PowerEdge R640, 112 vCPU, RAM 384GB, Local Storage 0GB, Ubuntu 22.04 LTS

## Validation Evidences

### Evidence of management cluster deployment

```
ubuntu@bootstrap:~/sylva-core$ ./bin/kubectl get pods -A
NAMESPACE                   NAME                                                             READY   STATUS      RESTARTS      AGE
calico-system               calico-kube-controllers-868d8d5ccb-b297r                         1/1     Running     0             12d
calico-system               calico-node-ggbl5                                                1/1     Running     0             12d
calico-system               calico-node-k8g5t                                                1/1     Running     0             12d
calico-system               calico-node-rmtst                                                1/1     Running     0             12d
calico-system               calico-node-xk2nk                                                1/1     Running     0             12d
calico-system               calico-typha-6494994658-gv4mc                                    1/1     Running     0             12d
calico-system               calico-typha-6494994658-wn6m8                                    1/1     Running     0             12d
capi-rancher-import         capi-rancher-import-6745974b77-z2rrt                             1/1     Running     0             12d
capi-system                 capi-controller-manager-59cdc55984-r5dxw                         1/1     Running     3 (19h ago)   12d
capm3-system                capm3-controller-manager-5d56789fc7-646fx                        1/1     Running     0             12d
capm3-system                ipam-controller-manager-7bddcb565c-xqmws                         1/1     Running     0             12d
capo-system                 capo-controller-manager-67dc49c6b-2qflx                          1/1     Running     2 (13m ago)   12d
cattle-fleet-local-system   fleet-agent-7b6499b8d6-7k79h                                     1/1     Running     0             12d
cattle-fleet-system         fleet-controller-95f44b688-h5n95                                 1/1     Running     0             12d
cattle-fleet-system         gitjob-774c4b9b57-ljflj                                          1/1     Running     0             12d
cattle-monitoring-system    alertmanager-rancher-monitoring-alertmanager-0                   2/2     Running     0             12d
cattle-monitoring-system    prometheus-rancher-monitoring-prometheus-0                       3/3     Running     0             107m
cattle-monitoring-system    rancher-monitoring-grafana-586df56bff-jcjz5                      3/3     Running     0             12d
cattle-monitoring-system    rancher-monitoring-kube-state-metrics-77ddfd789b-d26rv           1/1     Running     0             12d
cattle-monitoring-system    rancher-monitoring-operator-ff6cfcc76-ckn2m                      1/1     Running     0             12d
cattle-monitoring-system    rancher-monitoring-prometheus-adapter-79d8db9697-b2vvx           1/1     Running     0             12d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-gnvz4                1/1     Running     0             12d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-hjqn7                1/1     Running     0             12d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-jtq6j                1/1     Running     0             12d
cattle-monitoring-system    rancher-monitoring-prometheus-node-exporter-n45cj                1/1     Running     0             12d
cattle-system               rancher-fdc75c68-jbzfz                                           1/1     Running     0             12d
cattle-system               rancher-fdc75c68-zgjpq                                           1/1     Running     0             12d
cattle-system               rancher-fdc75c68-zhmks                                           1/1     Running     0             12d
cattle-system               rancher-webhook-649758c9b7-wp96k                                 1/1     Running     0             12d
cert-manager                cert-manager-74dc7f9649-gzb4b                                    1/1     Running     0             12d
cert-manager                cert-manager-cainjector-77457fbc49-qvfbg                         1/1     Running     0             12d
cert-manager                cert-manager-webhook-744bdd8c85-mf648                            1/1     Running     0             12d
cinder-csi                  openstack-cinder-csi-controllerplugin-6dc8bfd566-qb4tc           6/6     Running     6 (19h ago)   12d
cinder-csi                  openstack-cinder-csi-nodeplugin-5wcxn                            3/3     Running     0             12d
cinder-csi                  openstack-cinder-csi-nodeplugin-7h549                            3/3     Running     0             12d
cinder-csi                  openstack-cinder-csi-nodeplugin-jpwfv                            3/3     Running     0             12d
cinder-csi                  openstack-cinder-csi-nodeplugin-rz5hj                            3/3     Running     0             12d
cis-operator-system         cis-operator-6575fd686c-vtwnb                                    1/1     Running     0             12d
default                     check-rancher-clusters-job-fd2ld                                 0/1     Completed   0             2d2h
default                     keycloak-add-client-scope-k4hgq                                  0/1     Completed   0             12d
default                     pause-cluster-reconciliation-job-2dl9h                           0/1     Completed   0             2d5h
default                     pause-cluster-reconciliation-job-99l6k                           0/1     Completed   0             2d4h
default                     pause-cluster-reconciliation-job-bfztk                           0/1     Completed   0             12d
default                     pause-cluster-reconciliation-job-bnz8h                           0/1     Completed   0             2d4h
default                     pause-cluster-reconciliation-job-kdx2g                           0/1     Completed   0             2d2h
default                     pause-cluster-reconciliation-job-m9plb                           0/1     Completed   0             2d4h
default                     pause-cluster-reconciliation-job-ndtbp                           0/1     Completed   0             46h
default                     pause-cluster-reconciliation-job-npljm                           0/1     Completed   0             2d5h
default                     pause-cluster-reconciliation-job-q7fs4                           0/1     Completed   0             2d1h
default                     pause-cluster-reconciliation-job-vpfhv                           0/1     Completed   0             2d5h
default                     pause-cluster-reconciliation-job-ww7pr                           0/1     Completed   0             2d2h
default                     pause-cluster-reconciliation-job-zvl8j                           0/1     Completed   0             2d3h
default                     resume-cluster-reconciliation-job-59jb7                          0/1     Completed   0             2d1h
default                     resume-cluster-reconciliation-job-8gm5v                          0/1     Completed   0             2d4h
default                     resume-cluster-reconciliation-job-dl5jk                          0/1     Completed   0             2d4h
default                     resume-cluster-reconciliation-job-dnkxb                          0/1     Completed   0             2d5h
default                     resume-cluster-reconciliation-job-f9jqh                          0/1     Completed   0             2d5h
default                     resume-cluster-reconciliation-job-hwllh                          0/1     Completed   0             2d5h
default                     resume-cluster-reconciliation-job-k8h8g                          0/1     Completed   0             2d4h
default                     resume-cluster-reconciliation-job-nkdmb                          0/1     Completed   0             46h
default                     resume-cluster-reconciliation-job-tshpq                          0/1     Completed   0             2d2h
default                     resume-cluster-reconciliation-job-wd5zl                          0/1     Completed   0             2d3h
default                     resume-cluster-reconciliation-job-zmhps                          0/1     Completed   0             2d2h
external-secrets            external-secrets-operator-cert-controller-54b6f58d-nh75g         1/1     Running     0             12d
external-secrets            external-secrets-operator-db74c88fd-khc2p                        1/1     Running     0             12d
external-secrets            external-secrets-operator-webhook-69495c5b87-k7tcp               1/1     Running     0             12d
flux-system                 flux-webui-weave-gitops-7d67f5f4fc-w9xw9                         1/1     Running     0             12d
flux-system                 helm-controller-66dd588754-fk5rz                                 1/1     Running     0             12d
flux-system                 kustomize-controller-7c4bf6bf4b-6st8b                            1/1     Running     0             12d
flux-system                 notification-controller-55848b68ff-mx66k                         1/1     Running     0             12d
flux-system                 source-controller-f44b688dc-5skd4                                1/1     Running     0             12d
heat-operator-system        heat-operator-controller-manager-74884f684c-vxxrl                2/2     Running     3 (13m ago)   12d
k8s-gateway                 k8s-gateway-6cdd77c68-6rvr7                                      1/1     Running     0             12d
k8s-gateway                 k8s-gateway-6cdd77c68-s6pth                                      1/1     Running     0             12d
k8s-gateway                 k8s-gateway-6cdd77c68-vnphr                                      1/1     Running     0             12d
keycloak                    keycloak-0                                                       1/1     Running     0             12d
keycloak                    keycloak-operator-697b4c49bb-84kxr                               1/1     Running     0             12d
keycloak                    keycloak-realm-operator-6bc8f65c7f-724f7                         1/1     Running     0             12d
keycloak                    postgres-0                                                       1/1     Running     0             12d
keycloak                    sylva-l4r5g                                                      0/1     Completed   0             12d
kube-system                 etcd-management-cluster-cp-cf157bd6be-8pggg                      1/1     Running     0             12d
kube-system                 etcd-management-cluster-cp-cf157bd6be-p7p2w                      1/1     Running     0             12d
kube-system                 etcd-management-cluster-cp-cf157bd6be-pdc6c                      1/1     Running     0             12d
kube-system                 helm-install-rke2-calico-26fmn                                   0/1     Completed   1             12d
kube-system                 helm-install-rke2-calico-crd-xp5jw                               0/1     Completed   0             12d
kube-system                 helm-install-rke2-coredns-nz6xp                                  0/1     Completed   0             12d
kube-system                 helm-install-rke2-ingress-nginx-6vq2k                            0/1     Completed   0             12d
kube-system                 helm-install-rke2-metrics-server-7rhb8                           0/1     Completed   0             12d
kube-system                 kube-apiserver-management-cluster-cp-cf157bd6be-8pggg            1/1     Running     0             12d
kube-system                 kube-apiserver-management-cluster-cp-cf157bd6be-p7p2w            1/1     Running     0             12d
kube-system                 kube-apiserver-management-cluster-cp-cf157bd6be-pdc6c            1/1     Running     0             12d
kube-system                 kube-controller-manager-management-cluster-cp-cf157bd6be-8pggg   1/1     Running     1 (13m ago)   12d
kube-system                 kube-controller-manager-management-cluster-cp-cf157bd6be-p7p2w   1/1     Running     1 (9d ago)    12d
kube-system                 kube-controller-manager-management-cluster-cp-cf157bd6be-pdc6c   1/1     Running     0             12d
kube-system                 kube-proxy-management-cluster-cp-cf157bd6be-8pggg                1/1     Running     0             12d
kube-system                 kube-proxy-management-cluster-cp-cf157bd6be-p7p2w                1/1     Running     0             12d
kube-system                 kube-proxy-management-cluster-cp-cf157bd6be-pdc6c                1/1     Running     0             12d
kube-system                 kube-proxy-management-cluster-md-md0-4cd7368675-b2846            1/1     Running     0             12d
kube-system                 kube-scheduler-management-cluster-cp-cf157bd6be-8pggg            1/1     Running     1 (13m ago)   12d
kube-system                 kube-scheduler-management-cluster-cp-cf157bd6be-p7p2w            1/1     Running     1 (9d ago)    12d
kube-system                 kube-scheduler-management-cluster-cp-cf157bd6be-pdc6c            1/1     Running     0             12d
kube-system                 local-path-provisioner-798786479d-v8tdj                          1/1     Running     0             12d
kube-system                 rke2-coredns-rke2-coredns-58fd75f64b-57dnb                       1/1     Running     0             12d
kube-system                 rke2-coredns-rke2-coredns-58fd75f64b-kzf5x                       1/1     Running     0             12d
kube-system                 rke2-coredns-rke2-coredns-autoscaler-768bfc5985-h2b8b            1/1     Running     0             12d
kube-system                 rke2-ingress-nginx-controller-4r46h                              1/1     Running     0             12d
kube-system                 rke2-ingress-nginx-controller-62gjb                              1/1     Running     0             12d
kube-system                 rke2-ingress-nginx-controller-8jfs4                              1/1     Running     0             12d
kube-system                 rke2-ingress-nginx-controller-9w86w                              1/1     Running     0             12d
kube-system                 rke2-metrics-server-74f878b999-mgbt4                             1/1     Running     0             12d
kyverno                     kyverno-admission-controller-884f7957c-kvs8h                     1/1     Running     0             12d
kyverno                     kyverno-background-controller-658d5544f8-5v8qg                   1/1     Running     0             12d
kyverno                     kyverno-cleanup-admission-reports-28233520-zr7nw                 0/1     Completed   0             7m55s
kyverno                     kyverno-cleanup-cluster-admission-reports-28233520-czv9z         0/1     Completed   0             7m55s
kyverno                     kyverno-cleanup-controller-5cd69ccc4b-hpbbl                      1/1     Running     3 (13m ago)   12d
kyverno                     kyverno-reports-controller-754778bbbd-77vph                      1/1     Running     4 (47h ago)   12d
metal3-system               baremetal-operator-controller-manager-5b75967fc6-vtx7g           2/2     Running     2 (13m ago)   12d
metal3-system               ironic-5cf6b8549d-2bnk4                                          3/3     Running     0             12d
metal3-system               metal3-mariadb-primary-0                                         1/1     Running     0             12d
metal3-system               metal3-mariadb-secondary-0                                       1/1     Running     0             12d
metallb-system              helm-install-metallb-jv6w4                                       0/1     Completed   0             12d
metallb-system              metallb-controller-6bb94c65c5-j48zl                              1/1     Running     1 (12d ago)   12d
metallb-system              metallb-speaker-2vxl8                                            1/1     Running     0             12d
metallb-system              metallb-speaker-kkk4v                                            1/1     Running     0             12d
metallb-system              metallb-speaker-vcpdr                                            1/1     Running     0             12d
os-images                   os-image-server-ubuntu-2204-plain-qcow2-0                        1/1     Running     0             2d4h
rke2-bootstrap-system       rke2-bootstrap-controller-manager-6d5bd68958-6tffs               1/1     Running     3 (13m ago)   12d
rke2-control-plane-system   rke2-control-plane-controller-manager-757768647d-pnvb7           1/1     Running     3 (13m ago)   12d
tigera-operator             tigera-operator-7cc7df76d5-wlh2x                                 1/1     Running     4 (13m ago)   12d
vault                       vault-0                                                          2/2     Running     0             12d
vault                       vault-1                                                          2/2     Running     0             12d
vault                       vault-2                                                          2/2     Running     0             12d
vault                       vault-config-operator-857dcdbff4-fbwfk                           2/2     Running     1 (9d ago)    12d
vault                       vault-configurer-7bcc994975-6xxfb                                1/1     Running     0             12d
vault                       vault-operator-876f4b9d-mklvh                                    1/1     Running     3 (13m ago)   12d
```

### Evidence of bare metal workload deployment

```
ubuntu@bootstrap:~/sylva-core$ ./bin/kubectl get pods -A
NAMESPACE                  NAME                                                                     READY   STATUS      RESTARTS       AGE
calico-system              calico-kube-controllers-868d8d5ccb-4ctps                                 1/1     Running     0              2d2h
calico-system              calico-node-5z27s                                                        1/1     Running     0              2d
calico-system              calico-node-7kksv                                                        1/1     Running     0              2d
calico-system              calico-node-9bxgp                                                        1/1     Running     0              2d2h
calico-system              calico-node-b92gm                                                        1/1     Running     0              46h
calico-system              calico-node-phgkq                                                        1/1     Running     0              2d1h
calico-system              calico-typha-5f94f79b5c-cnrcx                                            1/1     Running     0              2d2h
calico-system              calico-typha-5f94f79b5c-hp9sk                                            1/1     Running     0              2d
calico-system              calico-typha-5f94f79b5c-mn22c                                            1/1     Running     0              46h
cattle-fleet-system        fleet-agent-847ffcbbb-m6qjt                                              1/1     Running     0              2d2h
cattle-monitoring-system   alertmanager-rancher-monitoring-alertmanager-0                           2/2     Running     0              2d2h
cattle-monitoring-system   prometheus-rancher-monitoring-prometheus-0                               3/3     Running     0              2d2h
cattle-monitoring-system   rancher-monitoring-grafana-586df56bff-bdfcl                              3/3     Running     0              2d2h
cattle-monitoring-system   rancher-monitoring-kube-state-metrics-77ddfd789b-cm2lf                   1/1     Running     0              2d2h
cattle-monitoring-system   rancher-monitoring-operator-ff6cfcc76-mthgz                              1/1     Running     0              2d2h
cattle-monitoring-system   rancher-monitoring-prometheus-adapter-79d8db9697-m9lnr                   1/1     Running     0              2d2h
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-227g5                        1/1     Running     0              2d
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-bb6mt                        1/1     Running     0              2d
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-d2rcb                        1/1     Running     0              2d1h
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-dbv9z                        1/1     Running     0              2d2h
cattle-monitoring-system   rancher-monitoring-prometheus-node-exporter-lgzzv                        1/1     Running     0              46h
cattle-sriov-system        sriov-574d587f59-zcvf5                                                   1/1     Running     0              2d2h
cattle-sriov-system        sriov-device-plugin-5lltg                                                1/1     Running     0              46h
cattle-sriov-system        sriov-device-plugin-snb8n                                                1/1     Running     0              46h
cattle-sriov-system        sriov-device-plugin-wdkq4                                                1/1     Running     0              46h
cattle-sriov-system        sriov-device-plugin-wqcbb                                                1/1     Running     0              46h
cattle-sriov-system        sriov-device-plugin-zcqkx                                                1/1     Running     0              46h
cattle-sriov-system        sriov-network-config-daemon-ftj7w                                        3/3     Running     0              2d
cattle-sriov-system        sriov-network-config-daemon-l9m9m                                        3/3     Running     0              2d
cattle-sriov-system        sriov-network-config-daemon-rr4pk                                        3/3     Running     0              2d2h
cattle-sriov-system        sriov-network-config-daemon-wthn2                                        3/3     Running     0              46h
cattle-sriov-system        sriov-network-config-daemon-xmxwp                                        3/3     Running     0              2d1h
cattle-sriov-system        sriov-rancher-nfd-master-7468d5b9c8-9kphg                                1/1     Running     0              2d2h
cattle-sriov-system        sriov-rancher-nfd-worker-6gjvj                                           1/1     Running     0              46h
cattle-sriov-system        sriov-rancher-nfd-worker-6jt64                                           1/1     Running     1 (2d2h ago)   2d2h
cattle-sriov-system        sriov-rancher-nfd-worker-8k9fg                                           1/1     Running     0              2d
cattle-sriov-system        sriov-rancher-nfd-worker-ssngm                                           1/1     Running     0              2d
cattle-sriov-system        sriov-rancher-nfd-worker-vflbw                                           1/1     Running     0              2d1h
cattle-system              cattle-cluster-agent-84d74d55bd-76pln                                    1/1     Running     0              2d2h
cattle-system              cattle-cluster-agent-84d74d55bd-jg7lb                                    1/1     Running     0              2d1h
cattle-system              rancher-webhook-744c69455f-lswn8                                         1/1     Running     0              2d2h
kube-system                etcd-first-workload-cluster-control-plane-5jpds                          1/1     Running     0              2d
kube-system                etcd-first-workload-cluster-control-plane-khljq                          1/1     Running     0              2d2h
kube-system                etcd-first-workload-cluster-control-plane-w2jhw                          1/1     Running     0              2d
kube-system                helm-install-rke2-calico-crd-9dzhd                                       0/1     Completed   0              2d2h
kube-system                helm-install-rke2-calico-ptx9g                                           0/1     Completed   1              2d2h
kube-system                helm-install-rke2-coredns-k8l9p                                          0/1     Completed   0              2d2h
kube-system                helm-install-rke2-ingress-nginx-hbwxl                                    0/1     Completed   0              2d2h
kube-system                helm-install-rke2-metrics-server-djtzg                                   0/1     Completed   0              2d2h
kube-system                kube-apiserver-first-workload-cluster-control-plane-5jpds                1/1     Running     0              2d
kube-system                kube-apiserver-first-workload-cluster-control-plane-khljq                1/1     Running     0              2d2h
kube-system                kube-apiserver-first-workload-cluster-control-plane-w2jhw                1/1     Running     0              2d
kube-system                kube-controller-manager-first-workload-cluster-control-plane-5jpds       1/1     Running     0              2d
kube-system                kube-controller-manager-first-workload-cluster-control-plane-khljq       1/1     Running     0              2d2h
kube-system                kube-controller-manager-first-workload-cluster-control-plane-w2jhw       1/1     Running     0              2d
kube-system                kube-proxy-first-workload-cluster-control-plane-5jpds                    1/1     Running     0              2d
kube-system                kube-proxy-first-workload-cluster-control-plane-khljq                    1/1     Running     0              2d2h
kube-system                kube-proxy-first-workload-cluster-control-plane-w2jhw                    1/1     Running     0              2d
kube-system                kube-proxy-first-workload-cluster-worker-md-md0-76cbf45b4dxdk84g-7897k   1/1     Running     0              2d1h
kube-system                kube-proxy-first-workload-cluster-worker-md-md0-76cbf45b4dxdk84g-tqxvn   1/1     Running     0              46h
kube-system                kube-scheduler-first-workload-cluster-control-plane-5jpds                1/1     Running     0              2d
kube-system                kube-scheduler-first-workload-cluster-control-plane-khljq                1/1     Running     0              2d2h
kube-system                kube-scheduler-first-workload-cluster-control-plane-w2jhw                1/1     Running     0              2d
kube-system                multus-ds-42fpj                                                          1/1     Running     0              2d2h
kube-system                multus-ds-ghrgc                                                          1/1     Running     0              2d
kube-system                multus-ds-h4wdw                                                          1/1     Running     0              2d1h
kube-system                multus-ds-r5dpc                                                          1/1     Running     0              46h
kube-system                multus-ds-rxpwj                                                          1/1     Running     0              2d
kube-system                multus-rke2-whereabouts-6csm5                                            1/1     Running     0              2d
kube-system                multus-rke2-whereabouts-9ctwm                                            1/1     Running     0              2d
kube-system                multus-rke2-whereabouts-nrdlb                                            1/1     Running     0              2d1h
kube-system                multus-rke2-whereabouts-rgx4w                                            1/1     Running     0              2d2h
kube-system                multus-rke2-whereabouts-rvgws                                            1/1     Running     0              46h
kube-system                rke2-coredns-rke2-coredns-58fd75f64b-97dbq                               1/1     Running     0              2d2h
kube-system                rke2-coredns-rke2-coredns-58fd75f64b-g2k25                               1/1     Running     0              46h
kube-system                rke2-coredns-rke2-coredns-58fd75f64b-nwgkq                               1/1     Running     0              2d1h
kube-system                rke2-coredns-rke2-coredns-autoscaler-768bfc5985-hnj7r                    1/1     Running     0              2d2h
kube-system                rke2-ingress-nginx-controller-4q855                                      1/1     Running     0              2d
kube-system                rke2-ingress-nginx-controller-4tplt                                      1/1     Running     0              2d2h
kube-system                rke2-ingress-nginx-controller-98px7                                      1/1     Running     0              46h
kube-system                rke2-ingress-nginx-controller-cgpzg                                      1/1     Running     0              2d1h
kube-system                rke2-ingress-nginx-controller-rz4d6                                      1/1     Running     0              2d
kube-system                rke2-metrics-server-74f878b999-kpstv                                     1/1     Running     0              2d2h
longhorn-system            csi-attacher-59db6bb96c-nhfjm                                            1/1     Running     0              2d2h
longhorn-system            csi-attacher-59db6bb96c-rbmq9                                            1/1     Running     0              2d2h
longhorn-system            csi-attacher-59db6bb96c-wwj7n                                            1/1     Running     0              2d2h
longhorn-system            csi-provisioner-6d44d79b88-jvg78                                         1/1     Running     0              2d2h
longhorn-system            csi-provisioner-6d44d79b88-r5slj                                         1/1     Running     0              2d2h
longhorn-system            csi-provisioner-6d44d79b88-stpqg                                         1/1     Running     0              2d2h
longhorn-system            csi-resizer-74cc7c9c95-bd4mj                                             1/1     Running     0              2d2h
longhorn-system            csi-resizer-74cc7c9c95-cr7kf                                             1/1     Running     0              2d2h
longhorn-system            csi-resizer-74cc7c9c95-nznkx                                             1/1     Running     0              2d2h
longhorn-system            csi-snapshotter-7f7685dfdb-9p5js                                         1/1     Running     0              2d2h
longhorn-system            csi-snapshotter-7f7685dfdb-wq8sg                                         1/1     Running     0              2d2h
longhorn-system            csi-snapshotter-7f7685dfdb-xdd7g                                         1/1     Running     0              2d2h
longhorn-system            engine-image-ei-ef01bf86-7kxvl                                           1/1     Running     0              2d1h
longhorn-system            engine-image-ei-ef01bf86-k2kxt                                           1/1     Running     0              2d
longhorn-system            engine-image-ei-ef01bf86-k97hx                                           1/1     Running     0              46h
longhorn-system            engine-image-ei-ef01bf86-rrtmq                                           1/1     Running     0              2d2h
longhorn-system            engine-image-ei-ef01bf86-zpz97                                           1/1     Running     0              2d
longhorn-system            instance-manager-e-2af9d38b137df413b7dc46fe86dbb4bc                      1/1     Running     0              2d2h
longhorn-system            instance-manager-e-3fb3485e7a9dc5b5dc9ef7d718af594a                      1/1     Running     0              2d
longhorn-system            instance-manager-e-53b964a44106fb6ee9aa44bec8456c23                      1/1     Running     0              2d
longhorn-system            instance-manager-e-66e80af38f04706171c7815f57b9f81e                      1/1     Running     0              2d1h
longhorn-system            instance-manager-e-bd2026fbef3d8eed190f3753015d2721                      1/1     Running     0              46h
longhorn-system            instance-manager-r-2af9d38b137df413b7dc46fe86dbb4bc                      1/1     Running     0              2d2h
longhorn-system            instance-manager-r-3fb3485e7a9dc5b5dc9ef7d718af594a                      1/1     Running     0              2d
longhorn-system            instance-manager-r-53b964a44106fb6ee9aa44bec8456c23                      1/1     Running     0              2d
longhorn-system            instance-manager-r-66e80af38f04706171c7815f57b9f81e                      1/1     Running     0              2d1h
longhorn-system            instance-manager-r-bd2026fbef3d8eed190f3753015d2721                      1/1     Running     0              46h
longhorn-system            longhorn-admission-webhook-5bf468b585-2p2jb                              1/1     Running     0              2d2h
longhorn-system            longhorn-admission-webhook-5bf468b585-8kmr5                              1/1     Running     0              2d2h
longhorn-system            longhorn-conversion-webhook-588cfff8b-h4rn6                              1/1     Running     0              2d2h
longhorn-system            longhorn-conversion-webhook-588cfff8b-t2xcd                              1/1     Running     0              2d2h
longhorn-system            longhorn-csi-plugin-5sfgs                                                3/3     Running     0              2d1h
longhorn-system            longhorn-csi-plugin-69n2t                                                3/3     Running     0              2d2h
longhorn-system            longhorn-csi-plugin-bcnp7                                                3/3     Running     0              2d
longhorn-system            longhorn-csi-plugin-m6jjq                                                3/3     Running     0              46h
longhorn-system            longhorn-csi-plugin-th5fd                                                3/3     Running     0              2d
longhorn-system            longhorn-driver-deployer-6f6dd986dc-74fwl                                1/1     Running     0              2d2h
longhorn-system            longhorn-manager-5m2bj                                                   1/1     Running     0              2d
longhorn-system            longhorn-manager-8xk6k                                                   1/1     Running     0              2d1h
longhorn-system            longhorn-manager-c2fxp                                                   1/1     Running     0              2d2h
longhorn-system            longhorn-manager-jkr4h                                                   1/1     Running     0              46h
longhorn-system            longhorn-manager-vhr47                                                   1/1     Running     0              2d
longhorn-system            longhorn-recovery-backend-66f74fdf9-5cbnv                                1/1     Running     0              2d2h
longhorn-system            longhorn-recovery-backend-66f74fdf9-g2fqr                                1/1     Running     0              2d2h
longhorn-system            longhorn-ui-7d959687bf-2p5x9                                             1/1     Running     0              2d2h
longhorn-system            longhorn-ui-7d959687bf-dcnhr                                             1/1     Running     0              2d2h
metallb-system             helm-install-metallb-g9xpn                                               0/1     Completed   0              2d2h
metallb-system             metallb-controller-6bb94c65c5-tl6k6                                      1/1     Running     1 (2d2h ago)   2d2h
metallb-system             metallb-speaker-kn58z                                                    1/1     Running     0              2d
metallb-system             metallb-speaker-tn5k5                                                    1/1     Running     0              2d2h
metallb-system             metallb-speaker-z4sj2                                                    1/1     Running     0              2d
tigera-operator            tigera-operator-7cc7df76d5-sh6vm                                         1/1     Running     0              2d2h

```

### Evidence of 6Wind vSR deployment
```
ubuntu@bootstrap:~/sylva-core$ ./bin/kubectl get pods -n vsr
NAME                   READY   STATUS    RESTARTS   AGE
vsr-658ccf89c5-95z8n   1/1     Running   0          25h
vsr-658ccf89c5-g5hf9   1/1     Running   0          25h
```

### Evidence of 6Wind vSR basic functional testing 
In order to validate vSR instantiation on Sylva Orange validation center (bare metal), 6Wind proposed to deploy and configure a simple setup and then to run 3 simple tests with the main objective to validate vSR on DPDK on SR-IOV on Sylva bare metal.

The setup is made of 2 vSR. Those 2 vSR are connected through an IPSec tunnel on top of DPDK/SR-IOV. Then we have to check that ping through the tunnel is working.

The 3 tests are:
- check SR-IOV interface is available inside vSR
- check fastpath is activated (DPDK)
- run a 'large' ping (number and size of packets). Check that pings reached the other vSR

#### SR-IOV check
Check SR-IOV interface is available inside vSR.
```
VSR1> show state / network-port 
network-port pci-b216s10f1
    bus-addr 0000:d8:0a.1
    vendor "Intel Corporation"
    model "Ethernet Virtual Function 700 Series"
    mac-address 26:8f:50:cc:43:97
    interface net1
    ..
```

#### DPDK check
Check fastpath is activated.
```
VSR1> show summary 
Service                   Status
=======                   ======

license                   enabled, valid (last server contact today)
product                   Virtual Service Router 3.6.1

fast-path                 enabled, 1 port, core-mask 28-55,84-111
linux                     112 cores, memory total 376.51GB available 335.68GB
network-port              1 port detected

vrf                       1 configured

interface loopback        enabled in vrf main (1 up iface)
interface physical        enabled in vrf main (1 up iface)
interface svti            enabled in vrf main (1 up iface)
interface system-loopback enabled in vrf main (1 up iface)
interface veth            enabled in vrf main (1 up iface)

ike                       enabled in vrf main (1 ike sa)
routing                   enabled in vrf main (5 ipv4 routes, 4 ipv6 routes)

auth                      2 users
dns                       enabled in vrf main
netconf-server            enabled in vrf main
```

#### Traffic through IPSec tunnel
```
VSR1> cmd ping 2.2.2.2 source 1.1.1.1 count 10000 rate 100 packetsize 32700

--- 2.2.2.2 ping statistics ---
10000 packets transmitted, 10000 received, 0% packet loss, time 146556ms
rtt min/avg/max/mdev = 1.906/26.452/58.272/9.979 ms, pipe 4

VSR1> show ike ike-sa details
vpn-demo: #1, ESTABLISHED, IKEv2, 1224deb812359033_i db64b126e02d2e4d_r
  local  'vsr1.6wind.net' @ 192.168.1.1[500]
  remote 'vsr2.6wind.net' @ 192.168.1.2[500]
  aes128-cbc/hmac-sha512/hmac-sha512/modp2048
  established 1768s ago, rekeying in 12163s
  trunk: #1, reqid 1, INSTALLED, TUNNEL, esp:aes128-cbc/hmac-sha256
    installed 1768s ago, rekeying in 1506s, expires in 2192s
    in  cdc774ec, svti-id 100, 327280000 bytes, 10000 packets
    out c6ea3ec1, svti-id 100, 327280000 bytes, 10000 packets
    local  0.0.0.0/0
    remote 0.0.0.0/0
```

## Validation feedback
This CNF raised a few difficulties that Sylva core team could be interrested in as a feedback.

### Push specific configuration on the worker.
6Wind vSR requires some specific worker configuration (MMU, huge pages, sysctl ...). This is the case for many CNF, especially if they use SR-IOV. We decided to use cloud-init "bootcmd" mechanism to do it.

Perhaps or more flexible solution using Sylva `values.yaml` would be usefull at least for the most common parameters.

### Allow Kubelet to instanciate "sysctl" PODs
Kubelet was not able to instanciate the vSR PODs because they claim the right to call "sysctl net.*". We had to manually edit the pod security policy (aka: PSP) to allow it.
`kubectl--kubeconfig=./workload-cluster-kubeconfigeditpspglobal-unrestricted-psp`

It could be usefull to allow a CNF to issue such calls using Sylva `values.yaml`.

### Allow containers to make "sysctl" calls
AppArmor was preventing vSR container to call `sysctl`. We had to allow it manually.
```
kind: Deployment
annotations:
  container.apparmor.security.beta.kubernetes.io/{{.Chart.Name}}: unconfined
```

It could be usefull to allow a CNF to issue such calls using Sylva `values.yaml`.

### Choose SR-IOV driver
6Wind vSR requires `vfio-pci` SR-IOV driver. On Sylva the worker OS default driver is used. As we have Ubuntu worker, `iavf` driver is used.
Hence, we had to manually unbind NIC from `iavf` driver and bind it to `vfio-pci` one.
```
echo0000:d8:02.0 > /sys/bus/pci/drivers/iavf/unbind
echo"vfio-pci" > /sys/bus/pci/devices/0000\:d8\:02.0/driver_override
echo0000:d8:02.0 > /sys/bus/pci/drivers_probe
```
The need to choose the NIC driver has been raised to Sylva dev team during the validation. This functionnality has already been [added](https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/740).
